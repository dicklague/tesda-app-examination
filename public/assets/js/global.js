$(document).ready(function(){
    $("#checkall").click(function () {
        if ($("input[type='checkbox']#checkall").is(':checked')) {
            $("input[type='checkbox'].checkbulk").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("input[type='checkbox'].checkbulk").each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $("[data-toggle='tooltip']").tooltip();

    $("input[type='checkbox'].checkbulk").click(function () {
       var totalCheckBoxes = $("input[type='checkbox'].checkbulk").length;
       var totalChecked = $("input[type='checkbox'].checkbulk:checked").length;

       if(totalChecked != totalCheckBoxes){
           $("input[type='checkbox']#checkall").prop("checked", false);
       } else {
           $("input[type='checkbox']#checkall").prop("checked", true);
       }
    });

    $(document).on("input","input.form-control",function(){
        if( $(this).hasClass('error') || ($(this).attr('required') && $(this).val() == "")){
            $(this).closest('div').removeClass('has-success');
            $(this).closest('div').addClass('has-error');
        } else if( $(this).hasClass('valid') ) {
            $(this).closest('div').removeClass('has-error');
            $(this).closest('div').addClass('has-success');
        }
    });
});

function showAlert(status,message){
    var alert = '';

    if( status.toLowerCase() == 'success'){
        alert = 'alert-success';
    } else if( status.toLowerCase() == 'error'){
        alert = 'alert-danger';
    } else if( status.toLowerCase() == 'info'){
        alert = 'alert-info';
    } else if( status.toLowerCase() == 'warning'){
        alert = 'alert-warning';
    }
    $("#alert-view").html('<div class="alert '+alert+' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong class="status">'+status.toUpperCase()+'!</strong> <span class="message">'+message+'</span>.</div>');
    $("#alert-view").removeClass('hide');

    return false;
}