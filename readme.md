## TESDA Simenar Web-based Examination System

## System Requirements
1.  Web Server
        1.  Apache
        2.  Mysql
        3.  PHP 5 or higher
2.  PHP JSON Extension

## Installation and Configuration
1.  git clone https://dicklague@bitbucket.org/dicklague/tesda-app-examination.git
2.  php composer update or php composer install
3.  php artisan migrate --package=cartalyst/sentry
4.  php artisan config:publish cartalyst/sentry
5.  php artisan migrate
6.  php artisan db:seed