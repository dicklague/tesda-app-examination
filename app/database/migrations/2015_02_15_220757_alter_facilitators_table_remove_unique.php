<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilitatorsTableRemoveUnique extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('facilitators', function(Blueprint $table)
		{
            $table->dropUnique('assessors_contact_number_secondary_unique');
            $table->dropColumn('contact_number_secondary');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('facilitators', function(Blueprint $table)
		{
			//
		});
	}

}
