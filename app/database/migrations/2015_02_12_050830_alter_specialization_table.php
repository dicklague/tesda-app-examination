<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSpecializationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('specializations', function(Blueprint $table)
		{
			$table->dropColumn('category');
			$table->integer('field_category_id')->unsigned();

			$table->foreign('field_category_id')
				->references('id')
				->on('field_category')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('specializations', function(Blueprint $table)
		{
			$table->dropColumn('field_category_id');
			$table->dropForeign('specializations_field_category_id_foreign');
		});
	}

}
