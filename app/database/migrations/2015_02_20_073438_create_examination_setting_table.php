<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExaminationSettingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('examination_settings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('type',25);
            $table->integer('specialization_id')->unsigned();
            $table->integer('number_of_items');
            $table->date('examination_date');
            $table->string('duration');
            $table->text('questions');
            $table->boolean('isActive')->default(1);
			$table->timestamps();
            $table->softDeletes();

            $table->foreign('specialization_id')
                ->references('id')
                ->on('specializations')
                ->onDelete('cascade');

            $table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('examination_settings');
	}

}
