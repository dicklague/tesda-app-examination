<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamineeActivationCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('examinee_activation_codes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('examinee_id')->unsigned();
            $table->integer('examination_setting_id')->unsigned();
            $table->string('username',36)->unique();
            $table->string('password',36)->unique();
            $table->dateTime('logged_in_at');
            $table->integer('login_ateempt')->unsigned()->default(0);

			$table->timestamps();
            $table->softDeletes();

            $table->foreign('examinee_id')
                ->references('id')
                ->on('examinees')
                ->onDelete('cascade');

            $table->foreign('examination_setting_id')
                ->references('id')
                ->on('examination_settings')
                ->onDelete('restrict');

            $table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('examinee_activation_codes');
	}

}
