<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('question_options', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('questions_id')->unsigned();
			$table->boolean('answer')->default(0);
			$table->text('option');
			$table->text('option_image');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('questions_id')
				->references('id')
				->on('questions')
				->onDelete('cascade');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('question_options');
	}

}
