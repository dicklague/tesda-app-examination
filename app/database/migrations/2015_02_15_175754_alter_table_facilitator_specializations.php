<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFacilitatorSpecializations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('facilitator_specializations', function(Blueprint $table)
		{
            $table->dropForeign('assessor_specializations_assessors_id_foreign');
            $table->dropForeign('assessor_specializations_specializations_id_foreign');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('facilitator_specializations', function(Blueprint $table)
		{
			//
		});
	}

}
