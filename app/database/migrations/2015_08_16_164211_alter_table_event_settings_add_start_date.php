<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEventSettingsAddStartDate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('event_settings', function(Blueprint $table)
		{
			$table->dateTime('start_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('event_settings', function(Blueprint $table)
		{
			$table->dropColumn('start_date');
		});
	}

}
