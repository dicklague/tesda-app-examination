<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assessors', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('users_id')->unsigned();
			$table->string('firstname',50);
			$table->string('lastname',50);
			$table->string('middlename',50);
			$table->string('extension_name',10);
			$table->string('gender',10);
			$table->string('civilstatus',35);
			$table->string('email',255)->unique();
			$table->string('contact_number_primary',15)->unique();
			$table->string('contact_number_secondary',15)->unique();
			$table->text('avatar');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('users_id')
					->references('id')
					->on('users')
					->onDelete('cascade');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assessors');
	}

}
