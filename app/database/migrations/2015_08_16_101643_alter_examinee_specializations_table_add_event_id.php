<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExamineeSpecializationsTableAddEventId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('examinee_specializations', function(Blueprint $table)
		{
			$table->integer('event_settings_id')->unsigned();
			#$table->foreign('event_settings_id')
			#	->references('id')
			#	->on('event_settings')
			#	->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('examinee_specializations', function(Blueprint $table)
		{
			#$table->dropForeign('examinee_specializations_event_settings_id_foreign');
			$table->dropColumn('event_settings_id');
		});
	}

}
