<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamineesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('examinees', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->text('avatar');
			$table->string('firstname',50);
			$table->string('lastname',50);
			$table->string('middlename',50);
			$table->string('extension_name',10);
			$table->string('gender',10);
			$table->text('school');
			$table->string('contact_number',15);
			$table->timestamps();
			$table->softDeletes();

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('examinees');
	}

}
