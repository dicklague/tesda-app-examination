<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessorSpecializationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assessor_specializations', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();

			$table->integer('assessors_id')->unsigned();
			$table->integer('specializations_id')->unsigned();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('assessors_id')
				->references('id')
				->on('assessors')
				->onDelete('cascade');

			$table->foreign('specializations_id')
				->references('id')
				->on('specializations')
				->onDelete('cascade');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assessor_specializations');
	}

}
