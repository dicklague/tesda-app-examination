<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExamineeTableAddContactnumber extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('examinees', function(Blueprint $table)
		{
			$table->string('contact_number',15)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('examinees', function(Blueprint $table)
		{
			$table->dropColumn('contact_number');
		});
	}

}
