<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExaminationResultTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('examination_result', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
            $table->integer('examination_setting_id')->unsigned();
            $table->integer('specialization_id')->unsigned();
            $table->integer('examinee_id')->unsigned();
            $table->integer('number_of_items')->unsigned();
            $table->integer('score')->unsigned();
            $table->string('type',25);
            $table->dateTime('examination_date_time');
			$table->timestamps();
            $table->softDeletes();

            $table->foreign('examinee_id')
                ->references('id')
                ->on('examinees')
                ->onDelete('cascade');

            $table->foreign('specialization_id')
                ->references('id')
                ->on('specializations')
                ->onDelete('restrict');

            $table->foreign('examination_setting_id')
                ->references('id')
                ->on('examination_settings')
                ->onDelete('restrict');

            $table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('examination_result');
	}

}
