<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableQuestionAddFacilitator extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questions', function(Blueprint $table)
		{
			$table->integer('facilitator_id')->unsigned();

            $table->foreign('facilitator_id')
                ->references('id')->on('facilitators')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questions', function(Blueprint $table)
		{
            $table->dropForeign('questions_facilitator_id_foreign');
            $table->dropColumn('facilitator_id');
		});
	}

}
