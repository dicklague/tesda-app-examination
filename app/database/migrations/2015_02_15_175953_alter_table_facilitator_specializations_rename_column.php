<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFacilitatorSpecializationsRenameColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('facilitator_specializations', function(Blueprint $table)
		{
            $table->dropIndex('assessor_specializations_assessors_id_foreign');
            $table->dropIndex('assessor_specializations_specializations_id_foreign');
            $table->renameColumn('assessors_id', 'facilitators_id');
            $table->foreign('facilitators_id')
                ->references('id')
                ->on('facilitators')
                ->onDelete('cascade');

            $table->foreign('specializations_id')
                ->references('id')
                ->on('specializations')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('facilitator_specializations', function(Blueprint $table)
		{
			//
		});
	}

}
