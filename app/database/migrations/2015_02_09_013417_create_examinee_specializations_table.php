<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamineeSpecializationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('examinee_specializations', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('examinees_id')->unsigned();
			$table->integer('specializations_id')->unsigned();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('examinees_id')
				->references('id')
				->on('examinees')
				->onDelete('cascade');

			$table->foreign('specializations_id')
				->references('id')
				->on('specializations')
				->onDelete('cascade');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('examinee_specializations');
	}

}
