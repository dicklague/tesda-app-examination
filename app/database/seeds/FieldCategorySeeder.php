<?php

class FieldCategorySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('field_category')->delete();

        FieldCategory::create(array('name' => 'NC I'));
        FieldCategory::create(array('name' => 'NC II'));
        FieldCategory::create(array('name' => 'NC III'));
        FieldCategory::create(array('name' => 'NC IV'));
    }

}