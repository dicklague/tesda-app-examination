<?php

class SentryGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('groups')->delete();

		Sentry::getGroupProvider()->create(array(
	        'name'        => 'Assessors',
	        'permissions' => array(
	            'administrator' => 0,
	            'assessor' => 1,
	        )));

		Sentry::getGroupProvider()->create(array(
	        'name'        => 'Administrators',
	        'permissions' => array(
	            'administrator' => 1,
	            'assessor' => 1,
	        )));
	}

}