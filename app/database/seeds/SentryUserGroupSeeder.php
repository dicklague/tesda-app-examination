<?php

class SentryUserGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users_groups')->delete();

		$userUser = Sentry::getUserProvider()->findByLogin('assessor@assessor.com');
		$adminUser = Sentry::getUserProvider()->findByLogin('admin@admin.com');

		$userGroup = Sentry::getGroupProvider()->findByName('Assessors');
		$adminGroup = Sentry::getGroupProvider()->findByName('Administrators');

	    // Assign the groups to the users
	    $userUser->addGroup($userGroup);
	    $adminUser->addGroup($userGroup);
	    $adminUser->addGroup($adminGroup);
	}

}