<?php

class ExaminationEventSettingSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        //update Event Setting
        $eventSetting = ExaminationEventSetting::where('active','=',1)->update(array(
            'active' => 0
        ));

        $event = ExaminationEventSetting::create(array(
            'title' => 'TESDA SEMINAR ONE',
            'active' => 1,
            'start_date' => date('Y-m-d H:i:s')
        ));

        //update Examinee_specialization
        $examineeSpecialization = ExamineeSpecialization::where('event_settings_id','=','')->update(array(
            'event_settings_id' => 1
        ));
        //update Exam Setting
        $examSetting = ExaminationSetting::where('event_settings_id','=','')->update(array(
            'event_settings_id' => 1
        ));
        //Update Exam Result
        $examResult = ExaminationResult::where('event_settings_id','=','')->update(array(
            'event_settings_id' => 1
        ));
    }

}