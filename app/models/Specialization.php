<?php

class Specialization extends Eloquent{
    protected $table = 'specializations';
    protected $fillable = array('field_category_id','title','description');
    protected $guarded = array('id');

    public function scopegetTitle($query, $id){
        return $this->where('id','=',$id)->first()->title;
    }

    public function scopegetName($query,$id){
        $response = $this->where('id','=',$id)->first();
        $string = '';
        $string .= FieldCategory::fieldname($response->field_category_id);
        $string .= ' - ';
        $string .= $response->title;
        return $string;
    }

    public function examinee()
    {
        return $this->hasMany('ExamineeSpecialization','specializations_id','id');
    }

    public function getSpecialization(){
        return FieldCategory::fieldname($this->field_category_id) .' - '.$this->title;
    }

    public function field(){
        return $this->belongsTo('FieldCategory','field_category_id');
    }

    public function question()
    {
        return $this->hasMany('Question','specializations_id','id');
    }

    public function examinationSetting()
    {
        return $this->hasMany('ExaminationSetting','specialization_id','id');
    }
}