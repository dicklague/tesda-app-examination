<?php

class ExaminationResult extends Eloquent{
    protected $table = 'examination_result';
    protected $fillable = array('examination_setting_id', 'specialization_id', 'examinee_id', 'number_of_items', 'score', 'type', 'examination_date_time','event_settings_id');
    protected $guarded = array('id');
}