<?php

class FacilitatorSpecialization extends Eloquent{
    protected $table = 'facilitator_specializations';
    protected $fillable = array('facilitators_id', 'specializations_id');
    protected $guarded = array('id');

    public function getId(){
        return $this->id;
    }

    public function getSpecializationId(){
        return $this->specialization_id;
    }

    public function specialization(){
        return $this->belongsTo('Specialization','specializations_id','id');
    }

    public function examinee()
    {
        return $this->belongsTo('Facilitator','facilitators_id','id');
    }
}