<?php

class ExamineeSpecialization extends Eloquent{
    protected $table = 'examinee_specializations';
    protected $fillable = array('examinees_id', 'specializations_id','event_settings_id');
    protected $guarded = array('id');

    public function getId(){
        return $this->id;
    }

    public function getSpecializationId(){
        return $this->specialization_id;
    }

    public function specialization(){
        return $this->belongsTo('Specialization','specializations_id','id');
    }

    public function eventsetting(){
        return $this->belongsTo('ExaminationEventSetting','event_settings_id','id');
    }

    public function examinee()
    {
        return $this->belongsTo('Examinee','examinees_id','id');
    }

    public static function getList($event_id){
        return ExamineeSpecialization::where('event_settings_id','=',$event_id)->get();
    }
}