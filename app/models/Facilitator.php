<?php

/**
 * Class Facilitator
 */
class Facilitator extends Eloquent{
    protected $table = 'facilitators';
    protected $fillable = array('users_id', 'firstname', 'lastname', 'middlename', 'extension_name', 'gender', 'civilstatus', 'email', 'contact_number_primary', 'contact_number_secondary', 'avatar');
    protected $guarded = array('id');

    /**
     * @return integer
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstname(){
        return $this->firstname;
    }

    public function getLastname(){
        return $this->lastname;
    }

    public function specialization()
    {
        return $this->hasMany('FacilitatorSpecialization','facilitators_id','id');
    }

    public function credential()
    {
        return $this->belongsTo('User', 'users_id');
    }

    public function question()
    {
        return $this->hasMany('Question','facilitator_id','id');
    }
}