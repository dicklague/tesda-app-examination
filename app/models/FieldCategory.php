<?php

class FieldCategory extends Eloquent{
    protected $table = 'field_category';
    protected $fillable = array('name');
    protected $guarded = array('id');

    public function scopefieldname($query,$id){
        return $this->where('id','=',$id)->first()->name;
    }

    public function specialization(){
        return $this->hasMany('Specialization','field_category_id','id');
    }
}