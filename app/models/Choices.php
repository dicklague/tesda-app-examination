<?php

class Choices extends Eloquent{
    protected $table = 'question_options';
    protected $fillable = array('questions_id', 'answer', 'option', 'option_image');
    protected $guarded = array('id');

    public function question(){
        return $this->belongsTo('Question','questions_id','id');
    }
}