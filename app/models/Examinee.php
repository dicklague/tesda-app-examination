<?php

class Examinee extends Eloquent{
    protected $table = 'examinees';
    protected $fillable = array('avatar', 'firstname', 'lastname', 'middlename', 'extension_name', 'gender', 'school', 'contact_number','event_settings_id');
    protected $guarded = array('id');

    public static function getRegisteredExaminees($event_id = '',$params = array()){
        $specialization = ExamineeSpecialization::getList($event_id);
    }

    public function specialization()
    {
        return $this->hasMany('ExamineeSpecialization','examinees_id','id');
    }

    public function activationcode(){
        return $this->hasMany('ActivationCode','examinee_id','id');
    }

    public static function name($id, $type = 'string'){
        $examinee = Examinee::find($id);
        if($type === 'array') {
            return array('lastname' => $examinee->lastname, 'firstname' => $examinee->firstname);
        } else {
            return $examinee->lastname.', '.$examinee->firstname;
        }
    }
}