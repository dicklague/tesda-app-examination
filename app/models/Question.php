<?php

class Question extends Eloquent{
    protected $table = 'questions';
    protected $fillable = array('facilitator_id', 'specializations_id', 'question', 'image_question');
    protected $guarded = array('id');

    public function choices(){
        return $this->hasMany('Choices','questions_id','id');
    }

    public function specialization(){
        return $this->belongsTo('Specialization','specializations_id','id');
    }

    public function facilitator(){
        return $this->belongsTo('Facilitator','facilitator_id','id');
    }

    public function answer(){
        return (string)Choices::where('questions_id','=',$this->id)->where('answer','=',1)->first()->option;
    }
}