<?php

class ActivationCode extends Eloquent{
    protected $table = 'examinee_activation_codes';
    protected $fillable = array('examinee_id', 'examination_setting_id', 'username', 'password', 'logged_in_at', 'login_ateempt');
    protected $guarded = array('id');

    public function examinee(){
        return $this->belongsTo('Examinee','examinee_id','id');
    }

    public function examinationsetting(){
        return $this->belongsTo('ExaminationSetting','examination_setting_id','id');
    }
}