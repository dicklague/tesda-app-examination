<?php

class ExaminationSetting extends Eloquent{
    protected $table = 'examination_settings';
    protected $fillable = array('type', 'specialization_id', 'number_of_items', 'examination_date', 'duration', 'questions', 'isActive','event_settings_id');
    protected $guarded = array('id');

    public function getQuestion(){
        return unserialize($this->questions);
    }

    public function eventsetting(){
        return $this->belongsTo('ExaminationEventSetting','event_settings_id','id');
    }

    public function specialization(){
        return $this->belongsTo('Specialization','specialization_id','id');
    }

    public function activationcode(){
        return $this->hasMany('ActivationCode','examination_setting_id','id');
    }
}