<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class ExaminationEventSetting extends Eloquent{
    use SoftDeletingTrait;

    protected $table = 'event_settings';
    protected $fillable = array('title','active','start_date');
    protected $guarded = array('id');
    protected $dates = ['deleted_at'];

    public static function countParticipant($event_id){
        return ExamineeSpecialization::where('event_settings_id',$event_id)->count();
    }

    public static function getActiveEvent(){
        $active = ExaminationEventSetting::where('active',1)->orderby('start_date','desc')->first();
        if ($active !== null) {
            return $active->id;
        } else {
            return null;
        }
    }
}