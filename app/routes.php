<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'ExaminationController@getLogin');
Route::group(array('prefix' => 'admin'), function()
{

	Route::get('/login','LoginController@getLogin');
	Route::post('/login','LoginController@postLogin');

    Route::group(array('before'=>'auth.admin'),function() {

        Route::get('/logout', function () {
            Sentry::logout();
            return Redirect::to('admin/login');
        });

        Route::get('/','AdminController@getIndex');
        Route::get('result','AdminController@getResult');
        Route::get('print','AdminController@postPrint');
        Route::post('examinees/unregister','ExamineeController@postRemoveExaminee');
        Route::controller('facilitators', 'FacilitatorsController');
        Route::controller('examinees', 'ExamineeController');
        Route::controller('fields', 'SpecializationController');
        Route::controller('questions', 'QuestionController');
        Route::controller('examinations', 'ExaminationController');
        Route::controller('monitoring', 'ExaminationMonitoringController');
        Route::get('setup/list','EventSetupController@getList');
        route::post('setup/activate','EventSetupController@postActivate');
        Route::resource('setup', 'EventSetupController');
    });
});

Route::group(array('prefix' => 'exam'),function(){
    Route::get('/','ExamController@getLogin');
	Route::get('login','ExamController@getLogin');
    Route::post('login','ExamController@postLogin');

    Route::group(array('before'=>'auth.examination'),function() {
        Route::get('overview', 'ExamController@getOverview');
        Route::post('start', 'ExamController@postStart');

        Route::get('question/{index}/{_token}', 'ExamController@getQuestion');
        Route::post('submit', 'ExamController@postAnswer');
        Route::get('checking/{_token}', 'ExamController@getChecking');
        Route::get('score/{_token}', 'ExamController@getScore');

        Route::post('sheet', 'ExamController@postSheet');

        Route::get('logout', 'ExamController@getLogout');
    });
});