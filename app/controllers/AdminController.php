<?php

class AdminController extends BaseController{

    public function getIndex(){
        if( !Sentry::check() ){
            return Redirect::to('admin/login');
        }
        $response['title'] = "Administrator Dashboard";
        $response['pagecontent'] = 'dashboard.index';
        $field = DB::select("select distinct examination_setting_id from examination_result where event_settings_id = ? AND COALESCE(COALESCE((score/number_of_items),0)*100,0) >= '70' ", array(ExaminationEventSetting::getActiveEvent()));

        $examinationSetting = array();
        foreach($field as $setting){
            $examinationSetting[] = ExaminationSetting::with('specialization')
                ->where('id','=',$setting->examination_setting_id)
                ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
                ->first();
        }

        $response['field'] = $examinationSetting;
        return View::make('admin.index',$response);
    }

    public function getAssessor(){
        $response['title'] = "Administrator Dashboard";
        $response['recordtitle'] = "Assessor";
        $response['pagecontent'] = 'assessor.index';
        return View::make('admin.index',$response);
    }

    public function getResult(){
        try {
            $input = Input::all();
            $id = $input['id'];
            $res = '';

            $result['list'] = $check_result = DB::select(DB::raw("SELECT * FROM examination_result WHERE COALESCE(COALESCE((score/number_of_items),0)*100,0) >= '70' AND examination_setting_id = '".$id."' AND event_settings_id = '".ExaminationEventSetting::getActiveEvent()."' order by score desc"));

            return View::make('dashboard.partials.list',$result);
        } catch(Exception $e){
            return array(
                'status' => false,
                'message' => $e->getMessage()
            );
        }
    }

    public function postPrint(){
        try {
            $input = Input::all();
            $id = $input['id'];
            $res = '';

            $result['title'] = 'Exmination Passers';
            $result['setting'] = ExaminationSetting::find($id);
            $result['list'] = $check_result = DB::select(DB::raw("SELECT * FROM examination_result WHERE COALESCE(COALESCE((score/number_of_items),0)*100,0) >= '70' AND examination_setting_id = '".$id."' AND event_settings_id = '".ExaminationEventSetting::getActiveEvent()."' order by score desc"));
            $result['specialization'] = Specialization::getName($result['setting']->specialization_id);
            $result['test'] = $result['setting']->type;
            return View::make('dashboard.partials.print',$result);
        } catch(Exception $e){
            return array(
                'status' => false,
                'message' => $e->getMessage()
            );
        }
    }
}