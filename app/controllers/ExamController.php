<?php

class ExamController extends BaseController{
    public function getLogin(){

        Session::forget('activation');
        Session::forget('score');
        Session::forget('initail_time');
        Session::forget('question');
        Session::forget('answers');
        Session::forget('examinee');
        Session::forget('setting');
        Session::forget('target_time');

        $response['title'] = 'Examination Process | Login';
        return View::make('examination.login',$response);
    }

    public function postLogin(){
        try {
            $credential = array(
                'username' => Input::get('examineeId'),
                'password' => Input::get('examineeCode')
            );

            $auth = ActivationCode::where('username','=',$credential['username'])->where('password','=',$credential['password'])
                                    ->first();
            if(count($auth) > 0){
                Session::put('activation',$auth);

                $setting = ExaminationSetting::find($auth->examination_setting_id);

                Session::put('setting', $setting);

                $examinee = Examinee::with('specialization')->find($auth->examinee_id);

                Session::put('examinee',$examinee);

                return Redirect::to('exam/overview');
            } else {
                return Redirect::to('exam/login')->with('status',false)->with('message','Invalid credentials');
            }

        } catch(Exception $e){
            return Redirect::to('exam/login')->with('status',false)->with('message','Invalid credentials');
        }
    }

    public function getOverview(){
        $response['title'] = 'Examination Process | Exam Overview';
        $response['examinee'] = json_decode(Session::get('examinee'));
        $response['setting'] = json_decode(Session::get('setting'));
        return View::make('examination.exam.overview',$response);
    }

    public function postStart(){
        try{
            $activation = json_decode(Session::get('activation'));
            $initialTime = strtotime("now");
            $setting = json_decode(Session::get('setting'));

            $question = $setting->questions;
            if(empty($question)){
                $question = Question::where('specializations_id','=',$setting->specialization_id)->lists('id');
                if($setting->number_of_items > count($question)){
                    Session::forget('activation');
                    Session::forget('score');
                    Session::forget('initail_time');
                    Session::forget('question');
                    Session::forget('answers');
                    Session::forget('examinee');
                    Session::forget('setting');
                    Session::forget('target_time');

                    return Redirect::to('exam/login')->with('status',false)
                                    ->with('message','Cannot Proceed!, Questions are not enough for the number of items');
                }
            } else {
                $question = unserialize($setting->questions);
                if($setting->number_of_items > count($question)){
                    $question = Question::where('specializations_id','=',$setting->specialization_id)->lists('id');
                }
            }

            shuffle($question);

            Session::put('initail_time',$initialTime);
            Session::put('question',serialize($question));

            $update_code = ActivationCode::find($activation->id);
            $update_code->logged_in_at = date("Y-m-d H:i:s");
            $update_code->save();

            $actualDate = time("now");

            $dateFormat = "Y-m-d H:i:s";
            $dateFormatyear = "Y";
            $dateFormatmonth = "m";
            $dateFormatday = "d";
            $dateFormattime = "H:i:s";
            $dateFormathour = "H";
            $dateFormatmin = "i";
            $dateFormatsec = "s";

            $tartime = strtotime("now + $setting->duration");

            // Define your target date here
            $targetYear  = date($dateFormatyear,$tartime);
            $targetMonth = date($dateFormatmonth,$tartime);
            $targetDay   = date($dateFormatday,$tartime);
            $targetHour  = date($dateFormathour,$tartime);
            $targetMinute= date($dateFormatmin,$tartime);
            $targetSecond= date($dateFormatsec,$tartime);
            // End target date definition

            // Define date format
            $targetDate = mktime($targetHour,$targetMinute,$targetSecond,$targetMonth,$targetDay,$targetYear);
            Session::put('target_time',$targetDate);

            return Redirect::to('exam/question/1/'.csrf_token());
        } catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function getQuestion($index,$_token){
        $examinee = json_decode(Session::get('examinee'));
        $setting = json_decode(Session::get('setting'));
        $number_of_items = $setting->number_of_items;
        $question = Session::get('question');
        $question = unserialize($question);
        $current_page = $index;
        if($index > 0){ $index = $index - 1; }
        $item = $question[$index];

        $detail = Question::with('choices')->where('id','=',$item)->first();
        $option = $detail->choices()->orderByRaw("RAND()")->get();

        $response['examinee'] = $examinee;
        $response['setting'] = $setting;
        $response['question'] = $detail;
        $response['choices'] = $option;
        $response['current_page'] = $current_page;
        $response['next_page'] = ($current_page < $number_of_items) ? $current_page + 1: 'checking';
        $response['title'] = 'Examination | Question '.$current_page;

        $actime = time("now");

        if(Session::has('target_time')) {
            $tartime = Session::get('target_time');
        } else {
            $dateFormat = "Y-m-d H:i:s";
            $dateFormatyear = "Y";
            $dateFormatmonth = "m";
            $dateFormatday = "d";
            $dateFormattime = "H:i:s";
            $dateFormathour = "H";
            $dateFormatmin = "i";
            $dateFormatsec = "s";

            $tartime = strtotime("now + $setting->duration");

            // Define your target date here
            $targetYear  = date($dateFormatyear,$tartime);
            $targetMonth = date($dateFormatmonth,$tartime);
            $targetDay   = date($dateFormatday,$tartime);
            $targetHour  = date($dateFormathour,$tartime);
            $targetMinute= date($dateFormatmin,$tartime);
            $targetSecond= date($dateFormatsec,$tartime);
            // End target date definition

            // Define date format
            $targetDate = mktime($targetHour,$targetMinute,$targetSecond,$targetMonth,$targetDay,$targetYear);
            Session::put('target_time',$targetDate);
            $tartime = $targetDate;
        }

        $secondsDiff = $tartime - $actime;

        if($secondsDiff > 0)
        {
            $remainingDay     = floor($secondsDiff/60/60/24);
            $remainingHour    = floor(($secondsDiff-($remainingDay*60*60*24))/60/60);
            $remainingMinutes = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))/60);
            $remainingSeconds = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))-($remainingMinutes*60));
        }
        if($secondsDiff < 0)
        {
            $remainingDay     = 0;
            $remainingHour    = 0;
            $remainingMinutes = 0;
            $remainingSeconds = 0;
        }

        $response['remainingDay']     = $remainingDay;
        $response['remainingHour']    = $remainingHour;
        $response['remainingMinutes'] = $remainingMinutes;
        $response['remainingSeconds'] = $remainingSeconds;

        return View::make('examination.exam.question',$response);
    }

    public function postAnswer(){
        try{
            $setting = json_decode(Session::get('setting'));
            $ans = array();
            if(Session::has('answers')){
                $ans = Session::get('answers');
            }

            $ans[Input::get('current_page')] = array(
                'question_id' => Input::get('questionId'),
                'choice_id' => Input::get('option')
            );
            Session::put('answers',$ans);

            if($setting->number_of_items == Input::get('current_page')){
                $this->_checkAnswer();
                return Redirect::to('exam/' . Input::get('next_page') . '/' . csrf_token());
            } else {
                return Redirect::to('exam/question/' . Input::get('next_page') . '/' . csrf_token());
            }
        } catch(Exception $e){
            return Redirect::to('exam/question/'.Input::get('current_page').'/'.csrf_token());
        }
    }

    public function getChecking($_token){
        if(!Session::has('score')){
            $this->_checkAnswer();
        }
        $response['title'] = 'Examination Process | Examination Checking';
        $response['examinee'] = json_decode(Session::get('examinee'));
        $response['setting'] = json_decode(Session::get('setting'));
        $response['answer'] = Session::get('answers');
        $response['score'] = Session::get('score');

        return View::make('examination.exam.checking',$response);
    }

    public function getScore($_token){
        if(!Session::has('score')){
            $this->_checkAnswer();
        }
        $response['title'] = 'Examination Process | Examination Checking';
        $response['examinee'] = json_decode(Session::get('examinee'));
        $response['setting'] = json_decode(Session::get('setting'));
        $response['answer'] = Session::get('answers');
        $response['score'] = Session::get('score');

        return View::make('examination.exam.score',$response);
    }

    protected function _checkAnswer(){
        $examinee = json_decode(Session::get('examinee'));
        $setting = json_decode(Session::get('setting'));
        $ans = Session::get('answers');
        $correct = 0;
        foreach ($ans as $key => $value) {
            $choice = Choices::find($value['choice_id']);
            if($choice->answer == 1){
                $correct = $correct + 1;
            }
        }

        $score = array(
            'examination_setting_id' => $setting->id,
            'specialization_id' => $setting->specialization_id,
            'examinee_id' => $examinee->id,
            'number_of_items' => $setting->number_of_items,
            'score' => $correct,
            'type' => $setting->type,
            'examination_date_time' => date("Y-m-d H:i:s"),
            'event_settings_id' => ExaminationEventSetting::getActiveEvent()
        );

        $score_save = ExaminationResult::create($score);

        Session::put('score',$score);

        $percentage = (double)(($correct/$setting->number_of_items) * 100);
        if($percentage >= 70){
            $removeActivation = ActivationCode::where('examination_setting_id','=',$setting->id)->where('examinee_id',$examinee->id)->delete();
        }
    }

    public function getLogout(){
        Session::forget('activation');
        Session::forget('score');
        Session::forget('initail_time');
        Session::forget('question');
        Session::forget('answers');
        Session::forget('examinee');
        Session::forget('setting');
        Session::forget('target_time');

        return Redirect::to('exam/login');
    }
}