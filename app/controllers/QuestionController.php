<?php

class QuestionController extends BaseController{
    public function getIndex($id = ''){
        $response['title'] = "Administrator | Question Bank";
        $response['recordtitle'] = "Question";
        $response['pagecontent'] = 'question.index';
        $response['fields'] = Specialization::all();
        $response['active'] = (!empty($id) ? $id : (is_object(Specialization::first())? Specialization::first()->id: null));
        $response['page'] = 'viewlist';
        $response['page_content_view'] = 'question.partials.questions';
        return View::make('admin.index',$response);
    }

    public function getViewlist($id){
        return $this->getIndex($id);
    }

    public function getList($id = ''){
        $id = !empty($id) ? $id: Specialization::first()->id;
        $response['list'] = Question::with('choices','facilitator','specialization')->where('specializations_id','=',$id)->get();
        return View::make('question.partials.list',$response);
    }

    public function getCreate($id = ''){
        $response['title'] = "Administrator | Question Bank";
        $response['recordtitle'] = "Question";
        $response['pagecontent'] = 'question.index';
        $response['fields'] = Specialization::all();
        $response['active'] = (!empty($id) ? $id : Specialization::first()->id);
        $response['page_content_view'] = 'question.partials.create';
        $response['page'] = 'create';
        return View::make('admin.index',$response);
    }

    public function postCreate($id = ''){
        DB::beginTransaction();
        try{
            $input = Input::all();

            $question = new Question;
            $question->question = trim($input['question']);
            $question->specializations_id = $input['field_of_specialization'];
            $question->facilitator_id = $this->_facilitator()->id;
            $question->save();

            foreach($input['option'] as $key => $choices){
                if(!empty($choices)) {
                    $option[$key] = new Choices;
                    $option[$key]->questions_id = $question->id;
                    $option[$key]->option = $choices;

                    if ((int)$key === (int)$input['answer']) {
                        $option[$key]->answer = 1;
                    } else {
                        $option[$key]->answer = 0;
                    }

                    $option[$key]->save();
                }
            }

            DB::commit();
            return Redirect::to('admin/questions/create/'.$input['field_of_specialization'])
                            ->with('alert_status',true)
                            ->with('alert_status_view','alert-success')
                            ->with('alert_status_message','<strong>Success! </strong>Question has been successfully saved!');

        } catch(Exception $e){
            DB::rollback();
            return Redirect::to('admin/questions/create/'.$input['field_of_specialization'])
                            ->with('alert_status',false)
                            ->with('alert_status_view','alert-danger')
                            ->with('alert_status_message','<strong>Error Code: '.$e->getCode().'! </strong> '.$e->getMessage().'!')
                            ->withInput();
        }
    }

    public function getData($id){

    }

    public function getUpdate($id,$question){
        $response['title'] = "Administrator | Question Bank";
        $response['recordtitle'] = "Question";
        $response['pagecontent'] = 'question.index';
        $response['fields'] = Specialization::all();
        $response['active'] = (!empty($id) ? $id : Specialization::first()->id);
        $response['page_content_view'] = 'question.partials.create';
        $response['page'] = 'viewlist';
        $response['data'] = Question::with('choices','facilitator','specialization')->find($question);
        return View::make('admin.index', $response);
    }

    public function postUpdate($id,$question){
        DB::beginTransaction();
        try{
            $input = Input::all();

            $question = Question::find($input['question_id']);
            $question->question = trim($input['question']);
            $question->save();

            Choices::where('questions_id','=',$question->id)->delete();

            foreach($input['option'] as $key => $choices){
                $choices = (string)$choices;
                if(!strlen($choices) == 0) {
                    $option[$key] = new Choices;
                    $option[$key]->questions_id = $question->id;
                    $option[$key]->option = $choices;

                   if ((int)$key === (int)$input['answer']) {
                       $option[$key]->answer = 1;
                    } else {
                        $option[$key]->answer = 0;
                   }

                    $option[$key]->save();
                }
            }

            DB::commit();
            return Redirect::to('admin/questions/viewlist/'.$input['field_of_specialization'])
                ->with('alert_status',true)
                ->with('alert_status_view','alert-success')
                ->with('alert_status_message','<strong>Success! </strong>Question has been successfully saved!');

        } catch(Exception $e){
            DB::rollback();
            $response['data'] = Question::with('choices','facilitator','specialization')->find($question);

            return Redirect::to('admin/questions/update/'.$input['field_of_specialization'].'/'.$question)
                ->with('alert_status',false)
                ->with('alert_status_view','alert-danger')
                ->with('alert_status_message','<strong>Error Code: '.$e->getCode().'! </strong> '.$e->getMessage().'!')
                ->withInput();
        }
    }

    public function postDelete($id){
        $input = Input::all();
        $check = Question::find($id);
        if(count($check) > 0){
            if($check->delete()){
                return Response::json(array(
                    'status' => true,
                    'message' => 'Question has been successfully deleted!',
                    'data' => $id
                ));
            } else {
                return Response::json(array(
                    'status' => false,
                    'message' => 'Unable to delete selected Question!',
                    'data' => $id
                ));
            }
        } else {
            return Response::json(array(
                'status' => false,
                'message' => 'Unable to delete selected Question!',
                'data' => $id
            ));
        }
    }
    public function getPrint(){
        $input = Input::all();
        $qid = explode(",",$input['qid']);
        $question = Question::with('choices')->whereIn('id',$qid)->get();
        $specialization = Specialization::find($input['fld']);

        return View::make('question.partials.print')
            ->with('qid',$question)
            ->with('specialization',$specialization);
    }
}