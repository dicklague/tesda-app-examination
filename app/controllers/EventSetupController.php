<?php

class EventSetupController extends \BaseController {
    public function __construct()
    {
        // Perform CSRF check on all post/put/patch/delete requests
        $this->beforeFilter('csrf', array('on' => array('post', 'put', 'patch', 'delete')));
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if( !Sentry::check() ){
			return Redirect::to('admin/login');
		}
		$response['title'] = "Administrator | Examination Setup";
		$response['recordtitle'] = "Examination Setup";
		$response['pagecontent'] = 'setup.index';
		return View::make('admin.index',$response);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();
        try {
            $active = (isset($input['active']) && $input['active'] == "1") ? true : false;
            if ($active) {
                $update = ExaminationEventSetting::where('active',1)->update(array('active'=>0));
            }
            $record = new ExaminationEventSetting();
            $record->title = $input['title'];
            $record->active = (isset($input['active']) && $input['active'] == "1") ? true : false;
            $record->start_date = date("Y-m-d H:i:s",strtotime($input['date']));
            $record->save();

            $response = array(
                'status' => true,
                'data' => $record,
                'message' => 'Successfully Set'
            );
            return Response::json($response);
        } catch(Exception $e){
            $error = array(
                'status' => false,
                'data' => $input,
                'message' => $e->getMessage()
            );
            return Response::json($error);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    /**
     * Return a collection of setup from storage.
     *
     * @return object
     */
    public function getList(){
        $response['list'] = ExaminationEventSetting::orderby('active','desc')->orderby('start_date','desc')->get();

        return View::make('setup.partials.list',$response);
    }

	public function postActivate(){
		DB::beginTransaction();
        $input = Input::all();
		try{
			$id = $input['_id'];

            $inactive_all = ExaminationEventSetting::where('active','=',1)->update(array('active'=>0));
            $activate_selected = ExaminationEventSetting::where('id','=',$id)->update(array('active'=>1));

            DB::commit();

            $success = array(
                'status' => false,
                'data' => $input,
                'message' => 'Successfully Activated'
            );
            return Response::json($success);
		}catch (Exception $e){
            DB::rollback();
			$error = array(
				'status' => false,
				'data' => $input,
				'message' => $e->getMessage()
			);
			return Response::json($error);
		}
	}
}
