<?php

class FacilitatorsController extends BaseController{
    public function getIndex(){
        $response['title'] = "Administrator | Facilitator Record";
        $response['recordtitle'] = "Facilitator";
        $response['pagecontent'] = 'facilitator.index';
        $response['fields'] = Specialization::all();
        return View::make('admin.index',$response);
    }

    public function getList(){
        $response['list'] = Facilitator::with('specialization')->with('credential')->get();

        return View::make('facilitator.partials.list',$response);
    }

    public function postCreate(){
        DB::beginTransaction();
        try {
            $input = Input::all();

	    $check_exists = Facilitator::where('firstname','=',Input::get('firstname'))->where('lastname','=',Input::get('lastname'))->count();
	    if($check_exists > 0){
		return Response::json(array(
				'status' => false,
				'message' => 'Facilitator was already exists'
			));
	    } else {

            	$data_user_account = array(
                	'email' => isset($input['email']) ? $input['email'] : NULL,
                	'password' => Hash::make(strtolower(str_ireplace(" ", "", $input['lastname']))),
                	'permissions' => NULL,
                	'activated' => 1,
                	'first_name' => isset($input['firstname']) ? $input['firstname'] : NULL,
                	'last_name' => isset($input['lastname']) ? $input['lastname'] : NULL,
            	);
            	$user_id = DB::table('users')->insertGetId($data_user_account);

            	$data_profile = array(
                	'users_id' => $user_id,
                	'firstname' => isset($input['firstname']) ? $input['firstname'] : NULL,
                	'lastname' => isset($input['lastname']) ? $input['lastname'] : NULL,
                	'middlename' => isset($input['middlename']) ? $input['middlename'] : NULL,
                	'extension_name' => isset($input['extension_name']) ? $input['extension_name'] : NULL,
                	'gender' => isset($input['gender']) ? $input['gender'] : NULL,
                	'civilstatus' => isset($input['civilstatus']) ? $input['civilstatus'] : NULL,
                	'email' => isset($input['email']) ? $input['email'] : NULL,
                	'contact_number_primary' => isset($input['contact']) ? $input['contact'] : NULL,
            	);
            	$profile_id = DB::table('facilitators')->insertGetId($data_profile);

            	$data_specialization = array(
                	'facilitators_id' => $profile_id,
                	'specializations_id' => isset($input['field']) ? $input['field'] : NULL,
            	);
            	DB::table('facilitator_specializations')->insert($data_specialization);

            	DB::commit();

            	return Response::json(array(
                	'status' => true,
                	'message' => 'New Facilitator has been added success'
            	));
	   }
        } catch(Exception $e){
            DB::rollback();
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function postUpdate($id){

    }

    public function postDelete($id){
        $input = Input::all();

        $faci = Facilitator::find($id);
        if( count($faci) > 0) {
            $check = User::find($faci->users_id);
            if (count($check) > 0) {
                if ($check->delete()) {
                    return Response::json(array(
                        'status' => true,
                        'message' => 'Record has been successfully deleted!',
                        'data' => $id
                    ));
                } else {
                    return Response::json(array(
                        'status' => false,
                        'message' => 'Unable to delete selected Record!',
                        'data' => $id
                    ));
                }
            } else {
                return Response::json(array(
                    'status' => false,
                    'message' => 'Record was not found!',
                    'data' => $id
                ));
            }
        } else {
            return Response::json(array(
                'status' => false,
                'message' => 'Record was not found!',
                'data' => $id
            ));
        }
    }

    public function getData($id){

    }
}
