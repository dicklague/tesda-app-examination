<?php
error_reporting(E_ALL);
class ExaminationController extends BaseController {

    public function getIndex(){
        $response['title'] = "Administrator | Examination Setting";
        $response['recordtitle'] = "Examination Setting";
        $response['pagecontent'] = 'examination.index';
        $response['fields'] = Specialization::all();
        $response['question'] = Question::all();
        $response['settings'] = ExaminationSetting::with('specialization')
            ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
            ->get();
        $response['view'] = 'setting';
        return View::make('admin.index',$response);
    }

    public function getLogin(){
        $response['title'] = 'Examination Process | Login';
        return View::make('examination.login',$response);
    }

    public function getSetting($id){
        try {
            if (empty($id)) {
                return Redirect::to('admin/examinations');
            }

            $settings = ExaminationSetting::with('specialization')->find($id);
            $response['title'] = "Administrator | Examination Setting";
            $response['recordtitle'] = "Examination Setting";
            $response['pagecontent'] = 'examination.index';
            $response['fields'] = Specialization::all();
            $response['question'] = Question::with('choices')->where('specializations_id', '=', $settings->specialization_id)->get();
            if (!empty($settings->questions)){
                $response['setquestion'] = unserialize($settings->questions);
            } else {
                $response['setquestion'] = array();
            }
            $response['settings'] = $settings;
            $response['view'] = 'question';
            return View::make('admin.index', $response);
        } catch(Exception $e){
            return Redirect::to('admin/examinations');
        }
    }

    public function postSetting(){
        try{
            $id = Input::get('settingId',NULL);

            $input = array(
                'type' => Input::get('examtype',NULL),
                'specialization_id' => Input::get('field_of_specialization',NULL),
                'number_of_items' => Input::get('number_of_items',NULL),
                'examination_date' => Input::get('examination_date',NULL),
                'duration' => Input::get('duration',NULL),
                'isActive' => Input::get('isActive',0),
                'event_settings_id' => ExaminationEventSetting::getActiveEvent()
            );

            if(empty($id)) {
                $check = ExaminationSetting::where('type', '=', $input['type'])
                    ->where('specialization_id', '=', $input['specialization_id'])
                    ->where('examination_date', '=', $input['examination_date'])
                    ->where('event_settings_id','=', ExaminationEventSetting::getActiveEvent())
                    ->first();
                if (count($check) > 0) {
                    return Redirect::to('admin/examinations')
                        ->with('alert_status', false)
                        ->with('alert_status_view', 'alert-warning')
                        ->with('alert_status_message', '<strong>Warning! </strong>Examination Setting already exists! <a class="alert-link" href="' . url('admin/examinations/setting/' . $check->id) . '">Update Settings</a>')
                        ->withInput();
                } else {
                    $questions = Question::where('specializations_id', '=', $input['specialization_id'])->count();
                    if ($questions < $input['number_of_items']) {
                        return Redirect::to('admin/examinations')
                            ->with('alert_status', false)
                            ->with('alert_status_view', 'alert-danger')
                            ->with('alert_status_message', '<strong>Failed! </strong>Not Enough Questions Stored in the Question Bank!')
                            ->withInput();
                    } else {

                        $setting = ExaminationSetting::create($input);
                        if ($setting) {
                            return Redirect::to('admin/examinations/setting/' . $setting->id)
                                ->with('alert_status', true)
                                ->with('alert_status_view', 'alert-success')
                                ->with('alert_status_message', '<strong>Success! </strong>Examination Setting has been saved, You can customized the questions to be rendered during the exam!');
                        } else {
                            return Redirect::to('admin/examinations')
                                ->with('alert_status', false)
                                ->with('alert_status_view', 'alert-danger')
                                ->with('alert_status_message', '<strong>Failed! </strong>Examination Setting has not been saved!')
                                ->withInput();
                        }
                    }
                }
            } else {
                $questions = Question::where('specializations_id', '=', $input['specialization_id'])->count();
                if ($questions < $input['number_of_items']) {
                    return Redirect::to('admin/examinations/setting/'.$id)
                        ->with('alert_status', false)
                        ->with('alert_status_view', 'alert-danger')
                        ->with('alert_status_message', '<strong>Failed! </strong>Not Enough Questions Stored in the Question Bank!')
                        ->withInput();
                } else {
                    $update = ExaminationSetting::where('id', '=', $id)->update($input);
                    if ($update) {
                        return Redirect::to('admin/examinations/setting/' . $id)
                            ->with('alert_status', true)
                            ->with('alert_status_view', 'alert-success')
                            ->with('alert_status_message', '<strong>Success! </strong>Examination Setting has been saved, You can customized the questions to be rendered during the exam!');
                    } else {
                        return Redirect::to('admin/examinations')
                            ->with('alert_status', false)
                            ->with('alert_status_view', 'alert-danger')
                            ->with('alert_status_message', '<strong>Failed! </strong>Examination Setting has not been saved!')
                            ->withInput();
                    }
                }
            }

        } catch(Exception $e){
            return Redirect::to('admin/examinations')
                ->with('alert_status',false)
                ->with('alert_status_view','alert-danger')
                ->with('alert_status_message','<strong>Failed! </strong>' . $e->getLine().' '. $e->getMessage())
                ->withInput();
        }
    }

    public function postSetquestion(){
        try {
            $question = Input::get('question');
            $question_count = count($question);
            $question = serialize($question);
            $id = Input::get('id');

            $update = ExaminationSetting::find($id);
            $update->questions = $question;
            if($update->save()){
                return Response::json(array(
                    'status' => true,
                    'message' => $question_count .' Selected Question has been set up for the examintion setting'
                ));
            } else {
                return Response::json(array(
                    'status' => false,
                    'message' => 'Cannot Update Setting'
                ));
            }
        }catch(Exception $e){
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
