<?php

class ExamineeController extends BaseController{
    public function getIndex(){
        $response['title'] = "Administrator | Examinee Registration";
        $response['recordtitle'] = "Examinee";
        $response['pagecontent'] = 'examinee.index';
        $response['fields'] = Specialization::all();
        $response['examinee_record'] = Examinee::with(array('specialization' => function($query){
            $query->where('event_settings_id','!=',ExaminationEventSetting::getActiveEvent());
        }))->orderby('lastname')->orderby('firstname')->get();
        return View::make('admin.index',$response);
    }

    public function getList(){
        $response['list'] = Examinee::with(array('specialization' => function($query){
            $query->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent());
        }))->orderby('lastname')->orderby('firstname')->get();

        return View::make('examinee.partials.list',$response);
    }

    public function postCreate(){

        try {
            $id = Input::get('id', '');
            $input = array(
                'firstname' => Input::get('firstname'),
                'lastname' => Input::get('lastname'),
                'middlename' => Input::get('middlename'),
                'extension_name' => Input::get('extension_name'),
                'gender' => Input::get('sex'),
                'school' => Input::get('school'),
                'contact_number' => Input::get('contact_number')
            );

            $specialization = Input::get('specialization');

            if (empty($id)) {
                $check = Examinee::where('firstname', '=', $input['firstname'])
                    ->where('lastname', '=', $input['lastname'])
                    ->where('gender', '=', $input['gender'])
                    ->count();
                if ($check > 0) {
                    return Response::json(array(
                        'status' => false,
                        'message' => 'Examinee was already Register'
                    ));
                } else {
                    $register = Examinee::create($input);
                    if ($register) {
                        $examinee_specialization = ExamineeSpecialization::create(array(
                            'examinees_id' => $register->id,
                            'specializations_id' => $specialization,
                            'event_settings_id' => ExaminationEventSetting::getActiveEvent()
                        ));
                        if ($examinee_specialization) {
                            return Response::json(array(
                                'status' => true,
                                'message' => 'New Examinee has been successfully registered.',
                                'data' => $register->id
                            ));
                        } else {
                            return Response::json(array(
                                'status' => false,
                                'message' => 'Unable to register new examinee.'
                            ));
                        }
                    } else {
                        return Response::json(array(
                            'status' => false,
                            'message' => 'Unable to register new examinee.'
                        ));
                    }
                }
            } else {
                $record = Examinee::find(Input::get('id'));
                $record->firstname = Input::get('firstname');
                $record->lastname = Input::get('lastname');
                $record->middlename = Input::get('middlename');
                $record->extension_name = Input::get('extension_name');
                $record->gender = Input::get('sex');
                $record->school = Input::get('school');
                $record->contact_number = Input::get('contact_number');

                if ($record->save()) {
                    $field = ExamineeSpecialization::where('examinees_id', '=', $record->id)->update(array(
                        'specializations_id' => $specialization
                    ));
                    return Response::json(array(
                        'status' => true,
                        'message' => 'Examinee record has been successfully updated.',
                        'data' => $record->id
                    ));
                } else {
                    return Response::json(array(
                        'status' => false,
                        'message' => 'Unable to save examinee record.'
                    ));
                }
            }

        } catch(Exception $e){
            return Response::json(array(
            'status' => false,
            'message' => $e->getMessage()
            ));
        }

    }

    public function getData($id){

    }

    public function postUpdate($id){

    }

    public function postDelete($id){
        $input = Input::all();
        $check = Examinee::find($id);
        if(count($check) > 0){
            if($check->delete()){
                return Response::json(array(
                        'status' => true,
                        'message' => 'Record has been successfully deleted!',
                        'data' => $id
                    ));
            } else {
                return Response::json(array(
                        'status' => false,
                        'message' => 'Unable to delete selected Record!',
                        'data' => $id
                    ));
            }
        } else {
            return Response::json(array(
                'status' => false,
                'message' => 'Unable to delete selected Record!',
                'data' => $id
            ));
        }
    }

    public function postRecord(){
        try{
            $record = Examinee::with('specialization')->find(Input::get('id'));
            return Response::json(array(
                'status' => true,
                'data' => $record
            ));
        } catch(Exception $e){
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function postRegisterexaminee(){
        DB::beginTransaction();
        try{
            $input = Input::all();
            $field = $input['field'];
            $id = explode(",",$input['id']);
            $exist = array();
            $success = array();
            foreach($id as $pa){
                $check = ExamineeSpecialization::where('examinees_id','=',$pa)
                    ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
                    ->count();

                $name = Examinee::find($pa);
                if($check <> 0){
                    $exist[] = ' - '.$name->firstname. ' ' .$name->lastname .' was already registered.';
                } else {
                    $sp = new ExamineeSpecialization();
                    $sp->examinees_id = $pa;
                    $sp->specializations_id = $field;
                    $sp->event_settings_id = ExaminationEventSetting::getActiveEvent();
                    $sp->save();

                    $success[] = ' + '. $name->firstname. ' ' .$name->lastname .' was successfully registered.';
                }
            }
            DB::commit();
            return Response::json(array(
                'status' => true,
                'data' => $input,
                'message' => implode('<br />',$exist).implode('<br />',$success)
            ));
        }catch (Exception $e){
            DB::rollback();
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function postRemoveExaminee(){
        DB::beginTransaction();
        try{
            $input = Input::all();

            $id = explode(",",$input['id']);

            $success = array();
            foreach($id as $pa){
                $check = ExamineeSpecialization::where('examinees_id','=',$pa)
                    ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
                    ->delete();

                $name = Examinee::find($pa);
                if($check <> 0){
                    $success[] = ' - '.$name->firstname. ' ' .$name->lastname .' was already unregistered.';
                }
            }
            DB::commit();
            return Response::json(array(
                'status' => true,
                'data' => $input,
                'message' => 'Participant was successfully Remove. You can reselect him from existing participants.'
            ));
        }catch(Exception $e){
            DB::rollback();
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}

