<?php

class LoginController extends BaseController{
    public function getIndex(){

    }

    public function getlogin(){
        $response['title'] = 'Examination Process | Login';
        return View::make('admin.login',$response);
    }

    public function postLogin(){
        $log = $this->_auth();
        return Response::json($log);
    }

    public function _auth(){
        try
        {
            $data = Input::all();
            // Login credentials
            $credentials = array(
                'email'    => $data['email'],
                'password' => $data['password'],
            );

            // Authenticate the user
            $user = Sentry::authenticate($credentials, true);
            $response = array(
                "message" => "Successfully Logged in",
                "status" => true,
                "user" => $user
            );

            return $response;
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            $message = 'Login field is required.';
            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            $message = 'Password field is required.';
            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }
        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
        {
            $message = 'Wrong password, try again.';
            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            $message = 'User was not found.';
            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }
        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            $message = 'User is not activated.';
            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }

        // The following is only required if the throttling is enabled
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
        {
            $time = $throttle->getSuspensionTime();
            $message = 'User is suspended.';
            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }
        catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            $message = 'User is banned.';

            $response = array(
                "message" => $message,
                "status" => false
            );

            return $response;
        }
    }
}