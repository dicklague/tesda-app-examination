<?php

class SpecializationController extends BaseController{
    public function getIndex(){
        $response['title'] = "Administrator | Field of Specialization";
        $response['recordtitle'] = "Field of Specialization";
        $response['pagecontent'] = 'specialization.index';
        $response['fieldcategory'] = FieldCategory::all();
        return View::make('admin.index',$response);
    }

    public function getList(){
        $response['list'] = Specialization::with('field')->get();
        return View::make('specialization.partials.list',$response);
    }

    public function postCreate(){
        try {

            $id = Input::get('id',NULL);
            $input = array(
                'field_category_id' => Input::get('category'),
                'title' => Input::get('name'),
                'description' => Input::get('description')
            );

            if(empty($id)) {
                $check = Specialization::where('field_category_id', '=', $input['field_category_id'])
                    ->where('title', '=', $input['title'])->count();

                if ($check > 0) {
                    $response = array('status' => false, 'message' => 'Record was already exists!');
                    return Response::json($response);
                } else {
                    $specialization = Specialization::create(array(
                        'field_category_id' => Input::get('category'),
                        'title' => Input::get('name'),
                        'description' => Input::get('description')
                    ));
                    $response = array('status' => true, 'message' => 'Record has been created!');
                    return Response::json($response);
                }
            } else {
                $check = Specialization::where('field_category_id', '=', $input['field_category_id'])
                    ->where('title', '=', $input['title'])
                    ->where('id','!=',$id)->count();
                if($check > 0){
                    $response = array('status' => false, 'message' => 'Record was already exists!');
                    return Response::json($response);
                } else {
                    $specialization = Specialization::find($id);
                    $specialization->field_category_id = $input['field_category_id'];
                    $specialization->title = $input['title'];
                    $specialization->description = $input['description'];
                    if($specialization->save()){
                        $response = array('status' => true, 'message' => 'Specialization record has been updated');
                        return Response::json($response);
                    } else {
                        $response = array('status' => false, 'message' => 'Specialization record cannot be updated');
                        return Response::json($response);
                    }
                }
            }
        } catch(Exception $e){
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function getData($id){

    }

    public function postUpdate($id){

    }

    public function postDelete($id){
        try {
            $input = Input::all();
            $check = Specialization::find($id);
            if (count($check) > 0) {
                if ($check->delete()) {
                    return Response::json(array(
                        'status' => true,
                        'message' => 'Record has been successfully deleted!',
                        'data' => $id
                    ));
                } else {
                    return Response::json(array(
                        'status' => false,
                        'message' => 'Unable to delete selected Record',
                        'data' => $id
                    ));
                }
            } else {
                return Response::json(array(
                    'status' => false,
                    'message' => 'Unable to delete selected Record',
                    'data' => $id
                ));
            }
        }catch (Exception $e){
            return Response::json(array(
                'status' => false,
                'message' => 'Unable to delete selected Record! Record was currently use by another Collection',
                'data' => $id
            ));
        }
    }

    public function getRecord(){
        try{
            $id = Input::get('id');
            $record = Specialization::find($id);
            return Response::json(array('status'=>true,'data'=>$record));
        } catch(Exception $e){
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}