<?php

class ExaminationMonitoringController extends BaseController{
    public function getIndex(){
        $response['title'] = "Administrator | Monitoring Examination";
        $response['recordtitle'] = "Examination Monitoring";
        $response['pagecontent'] = 'monitoring.index';
        $response['fields'] = Specialization::all();
        $response['examinee'] = Examinee::with('specialization')
            ->orderby('lastname','asc')
            ->orderby('firstname','asc')
            ->get();
        $response['settings'] = ExaminationSetting::with('specialization')
            ->where('isActive','=',1)
            ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
            ->get();
        $response['post_settings'] = ExaminationSetting::with('specialization')
            ->where('type','=','post test')
            ->where('isActive','=',1)
            ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
            ->get();
        $response['pre_settings'] = ExaminationSetting::with('specialization')
            ->where('type','=','pre test')
            ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
            ->where('isActive','=',1)->get();
        return View::make('admin.index',$response);
    }

    public function getActivation(){
        try{
            $examId = Input::get('id');
            $response['title'] = 'Activation Code';
            $response['activation'] = ActivationCode::with('examinee','examinationsetting')->where('examination_setting_id','=',$examId)->get();

            $setting = ExaminationSetting::find($examId);
            $response['specialization'] = Specialization::getName($setting->specialization_id);
            $response['test'] = ucfirst($setting->type);

            $pdf = PDF::loadView('monitoring.activationcode', $response);
            return $pdf->stream();
        } catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function postActivate(){
        DB::beginTransaction();
        try{
            $examId = '';
            $request = Input::get('activation');

            if(count($request) > 0){
                foreach($request as $key => $value){
                    $examId = $value['examinationId'];
                    $activate = new ActivationCode;
                    $activate->examinee_id = $value['id'];
                    $activate->examination_setting_id = $value['examinationId'];
                    $activate->username = $value['password'];
                    $activate->password = $value['username'];
                    $activate->save();
                }

                DB::commit();
                return Response::json(array(
                    'status' => true,
                    'message' => count($request) .' Examinee code has been activated',
                    'data' => $this->_getExamineeList($examId)
                ));
            } else {
                return Response::json(array(
                    'status' => false,
                    'message' => 'No Examinee were selected to activate'
                ));
            }
        } catch(Exception $e){
            DB::rollback();
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function getExaminee(){
        try {
            $id = Input::get('id');
            $setting = $response['settings'] = ExaminationSetting::with('specialization')
                ->where('id','=',$id)
                ->where('isActive','=',1)
                ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
                ->first();

            $res = '';

            if(count($setting) > 0){
                $examinee = DB::table('examinees')
                    ->leftJoin('examinee_specializations',function($join){
                                $join->on('examinees.id', '=', 'examinee_specializations.examinees_id');
                    })
                    ->where('examinee_specializations.specializations_id', '=', $setting->specialization_id)
                    ->where('examinee_specializations.event_settings_id', '=', ExaminationEventSetting::getActiveEvent())
                    ->orderby('examinees.lastname','asc')
                    ->orderby('examinees.firstname','asc')
                    ->get();

                foreach ($examinee as $key => $value) {
                    $check_result = DB::select(DB::raw("SELECT * FROM examination_result WHERE COALESCE(COALESCE((score/number_of_items),0)*100,0) >= '70' AND examinee_id = '".$value->examinees_id."' AND examination_setting_id = '".$setting->id."'"));

                    if(count($check_result) > 0){
                        $res .= '<tr id="' . $value->examinees_id . '" class="success">';
                        $res .= '<td></td>';
                        $res .= '<td>' . strtoupper($value->lastname) . '</td>';
                        $res .= '<td>,' . strtoupper($value->firstname . ' ' . $value->extension_name) . '</td>';
                        $res .= '<td>' . strtoupper(substr($value->middlename, 0, 1)) . '</td>';
                        $res .= '<td>' . Specialization::getName($value->specializations_id) . '</td>';
                        $res .= '<td class="text-code"></td>';
                        $res .= '<td class="text-code"></td>';
                        $res .= '<td class="text-center">[ '.$check_result[0]->score.'/'.$check_result[0]->number_of_items.' ] ( '.(($check_result[0]->score/$check_result[0]->number_of_items)*100).'% )Passed</td>';
                        $res .= '</tr>';
                    } else {

                        $code = DB::table('examination_settings')
                            ->leftJoin('examinee_activation_codes', function ($join) {
                                $join->on('examinee_activation_codes.examination_setting_id', '=', 'examination_settings.id');
                            })
                            ->where('examinee_activation_codes.examinee_id', '=', $value->examinees_id)
                            ->where('examination_settings.specialization_id', '=', $value->specializations_id)
                            ->where('examination_settings.id', '=', $setting->id)
                            ->first();
                        $username = str_random(5);
                        $password = str_random(5);
                        $res .= '<tr id="' . $value->examinees_id . '">';
                        $res .= '<td>';
                        $res .= ((count($code) > 0) ? '' : '<input data-username="' . $username . '" data-password="' . $password . '" data-examination-id="' . $setting->id . '" value="' . $value->examinees_id . '" type="checkbox" class="checkbulk" />');
                        $res .= '</td>';
                        $res .= '<td>' . strtoupper($value->lastname) . '</td>';
                        $res .= '<td>,' . strtoupper($value->firstname . ' ' . $value->extension_name) . '</td>';
                        $res .= '<td>' . strtoupper(substr($value->middlename, 0, 1)) . '</td>';
                        $res .= '<td>' . Specialization::getName($value->specializations_id) . '</td>';
                        $res .= '<td class="text-code">' . ((count($code) > 0) ? $code->username : $username) . '</td>';
                        $res .= '<td class="text-code">' . ((count($code) > 0) ? $code->password : $password) . '</td>';
                        $res .= '<td class="text-center">' . ((count($code) > 0) ? (($code->logged_in_at != '0000-00-00 00:00:00') ? $code->logged_in_at : '<i class="fa fa-check-circle"></i>') : '<i class="fa fa-question-circle"></i>') . '</td>';
                        $res .= '</tr>';
                    }
                }

            }
            return Response::json($res);
        } catch(Exception $e){
            return Response::json(array(
                'status' => false,
                'message' => $e->getMessage() . ' Line: '.$e->getLine()
            ));
        }
    }

    protected function _getExamineeList($id){
        try {
            $setting = $response['settings'] = ExaminationSetting::with('specialization')
                ->where('id','=',$id)
                ->where('isActive','=',1)
                ->where('event_settings_id','=',ExaminationEventSetting::getActiveEvent())
                ->first();

            $res = '';

            if(count($setting) > 0){
                $examinee = DB::table('examinees')
                    ->leftJoin('examinee_specializations',function($join){
                        $join->on('examinees.id', '=', 'examinee_specializations.examinees_id');
                    })
                    ->where('examinee_specializations.specializations_id', '=', $setting->specialization_id)
                    ->where('examinee_specializations.event_settings_id', '=', ExaminationEventSetting::getActiveEvent())
                    ->orderby('examinees.lastname','asc')
                    ->orderby('examinees.firstname','asc')
                    ->get();

                foreach ($examinee as $key => $value) {
                    $check_result = DB::select(DB::raw("SELECT * FROM examination_result WHERE COALESCE(COALESCE((score/number_of_items),0)*100,0) >= '70' AND examinee_id = '".$value->examinees_id."' AND examination_setting_id = '".$setting->id."'"));

                    if(count($check_result) > 0){
                        $res .= '<tr id="' . $value->examinees_id . '" class="success">';
                        $res .= '<td></td>';
                        $res .= '<td>' . strtoupper($value->lastname) . '</td>';
                        $res .= '<td>,' . strtoupper($value->firstname . ' ' . $value->extension_name) . '</td>';
                        $res .= '<td>' . strtoupper(substr($value->middlename, 0, 1)) . '</td>';
                        $res .= '<td>' . Specialization::getName($value->specializations_id) . '</td>';
                        $res .= '<td class="text-code"></td>';
                        $res .= '<td class="text-code"></td>';
                        $res .= '<td class="text-center">[ '.$check_result[0]->score.'/'.$check_result[0]->number_of_items.' ] ( '.(($check_result[0]->score/$check_result[0]->number_of_items)*100).'% )Passed</td>';
                        $res .= '</tr>';
                    } else {

                        $code = DB::table('examination_settings')
                            ->leftJoin('examinee_activation_codes', function ($join) {
                                $join->on('examinee_activation_codes.examination_setting_id', '=', 'examination_settings.id');
                            })
                            ->where('examinee_activation_codes.examinee_id', '=', $value->examinees_id)
                            ->where('examination_settings.specialization_id', '=', $value->specializations_id)
                            ->where('examination_settings.id', '=', $setting->id)
                            ->first();
                        $username = str_random(5);
                        $password = str_random(5);
                        $res .= '<tr id="' . $value->examinees_id . '">';
                        $res .= '<td>';
                        $res .= ((count($code) > 0) ? '' : '<input data-username="' . $username . '" data-password="' . $password . '" data-examination-id="' . $setting->id . '" value="' . $value->examinees_id . '" type="checkbox" class="checkbulk" />');
                        $res .= '</td>';
                        $res .= '<td>' . strtoupper($value->lastname) . '</td>';
                        $res .= '<td>,' . strtoupper($value->firstname . ' ' . $value->extension_name) . '</td>';
                        $res .= '<td>' . strtoupper(substr($value->middlename, 0, 1)) . '</td>';
                        $res .= '<td>' . Specialization::getName($value->specializations_id) . '</td>';
                        $res .= '<td class="text-code">' . ((count($code) > 0) ? $code->username : $username) . '</td>';
                        $res .= '<td class="text-code">' . ((count($code) > 0) ? $code->password : $password) . '</td>';
                        $res .= '<td class="text-center">' . ((count($code) > 0) ? (($code->logged_in_at != '0000-00-00 00:00:00') ? $code->logged_in_at : '<i class="fa fa-check-circle"></i>') : '<i class="fa fa-question-circle"></i>') . '</td>';
                        $res .= '</tr>';
                    }
                }

            }
            return $res;
        } catch(Exception $e){
            return array(
                'status' => false,
                'message' => $e->getMessage()
            );
        }
    }
}