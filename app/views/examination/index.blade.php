<div class="row">
    <div class="col-md-3">
        @include('examination.settings.create')
    </div>

    <div class="col-md-9">
        @if(isset($view) && $view == 'question')
            @include('examination.settings.question')
        @else
            @include('examination.settings.list')
        @endif
    </div>
</div>