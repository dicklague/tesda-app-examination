@extends('layout.master')

@section('page-view-css')
    @parent
    <style>
        #overview{
            margin-top: -50px !important;
        }
        #overview .panel-title{
            text-transform: uppercase;
            font-weight: bolder;
        }

        #question-display{
            margin: 10px;
            min-height: 300px;
            border-bottom: 1px solid #c9cccf;
        }

        #question-display p{
            text-indent: 25px;
        }

        .render-option{
            margin-bottom: 10px;
        }

        #question-option{
            list-style: none;
            padding:0px;
            margin: 10px;
        }

        #question-option li input[type='radio']{
            display: block;
            float: left;
            margin-right: 10px;
        }

        #question-option li{
            float:left;
            width:49.5%;
            padding:0px;
            margin:1px;
        }
        #question-option li:hover{
            background: #00b3ee;
        }
        #question-option li label{
            float:left;
            border: 1px solid #c9cccf;
            width: 100%;
            padding: 5px;
            cursor: pointer;
            margin: 0px;
        }
    </style>
@stop

@section('page-view-content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" id="overview">
                    <div class="panel-heading">
                        <h3 class="panel-title">Remaining Time: <span id="remain"></span></h3>
                    </div>
                    <div class="panel-body table-container">
                        <form id="examQuestionForm" action="{{url('exam/submit')}}" method="post">
                            {{Form::token()}}
                            <input type="hidden" name="current_page" value="{{$current_page}}" id="current_page" />
                            <input type="hidden" name="next_page" value="{{$next_page}}" id="next_page" />

                            <input type="hidden" name="questionId" value="{{$question->id}}" id="questionId" />
                            <fieldset id="question-display">
                                <legend>Question # {{$current_page}}</legend>
                                {{$question->question}}
                            </fieldset>
                            <div class="render-option">
                                <ul id="question-option">
                                @foreach($choices as $option)
                                    <li>
                                        <label for="option_{{$option->id}}">
                                            <div>
                                                <input type="radio" name="option" class="option" id="option_{{$option->id}}" value="{{$option->id}}"/>
                                            </div>
                                            <div>
                                                {{$option->option}}
                                            </div>
                                        </label>
                                    </li>
                                @endforeach
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <div id="alert-view" class="pull-left"></div>
                        <div class="btn-group pull-right">
                            @if($current_page == $setting->number_of_items)
                                <button type="submit" id="submitAnswer" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> View Checking</button>
                            @else
                                <button type="submit" id="submitAnswer" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Next Question</button>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-view-js')
    @parent
    <script>
        $(document).ready(function(){
            setCountDown();
        });
    </script>
    <script>
        $(function(){
            $(document).on("click","#submitAnswer",function(e){
                e.preventDefault();
                e.stopPropagation();

                var answer = $("input.option:checked").length;
                if(answer == 0){
                    showAlert('error','Please Select an answer');
                } else {
                    $("#examQuestionForm").submit();

                    var btn = $(this);
                    btn.find("i.fa").removeClass('fa-arrow-right');
                    btn.find("i.fa").addClass('fa-spinner').addClass('fa-spin');
                }
            });
        });
    </script>
    <script>
        window.history.forward(1);
    </script>
    <SCRIPT TYPE="text/javascript">
        <!--
        function clickIE() {if (document.all) {(message);return false;}}
        function clickNS(e) {if
        (document.layers||(document.getElementById&&!document.all)) {
            if (e.which==2||e.which==3||e.which==8||e.which==44) {(message);return false;}}}
        if (document.layers)
        {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
        else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
        document.oncontextmenu=new Function("return false")
        // -->
    </SCRIPT>
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).keyup(function(e){
                if(e.keyCode == 44){
                    e.preventDefault();
                    return false;
                }
            });
            $(window).keypress(function(e){
                if(e.keyCode != 116){
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
    <script type="text/javascript">
        var days = '{{$remainingDay}}';
        var hours = '{{$remainingHour}}';
        var minutes = '{{$remainingMinutes}}';
        var seconds = '{{$remainingSeconds}}';

         days = parseInt(days);
         hours = parseInt(hours);
         minutes = parseInt(minutes);
         seconds = parseInt(seconds);

                function setCountDown () {
                    if((seconds == 0) && (minutes == 0) && (hours == 0)){
                        seconds = 0;
                        minutes = 0;
                        hours = 0;
                        days = 0;
                    }else{
                        seconds--;
                        if (seconds < 0){
                            minutes--;
                            seconds = 59;
                        }
                        if (minutes < 0){
                            hours--;
                            minutes = 59;
                        }
                        if (hours < 0){
                            days--;
                            hours = 23;
                        }
                    }
                    document.getElementById("remain").innerHTML = hours+" hours, "+minutes+" min, "+seconds+" sec";
                    if((hours == 0) && (minutes == 0) && (seconds == 0))
                    {
                        window.alert("Time is up. Press OK to continue.");
                        window.location = "{{url('exam/score/'.csrf_token())}}";
                    } else {
                        setTimeout ( "setCountDown()", 1000 );
                    }
                }
    </script>
@stop