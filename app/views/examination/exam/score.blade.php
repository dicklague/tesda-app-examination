@extends('layout.master')

@section('page-view-css')
    @parent
    <style>
        #authLogin{
            margin-top: -50px !important;
        }
        #authLogin .panel-title{
            text-transform: uppercase;
            font-weight: bolder;
        }
    </style>
@stop

@section('page-view-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" id="authLogin">
                    <div class="panel-heading">
                        <h3 class="panel-title">Examination Score </h3>
                    </div>
                    <div class="panel-body table-container">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th style="width:180px;">Participant/Examinee</th>
                                <td style="text-transform: uppercase;">: {{$examinee->lastname}} ,{{$examinee->firstname}} {{$examinee->extension_name}} {{$examinee->middlename}}</td>
                            </tr><tr>
                                <th>Field of Specialization</th>
                                <td>: {{Specialization::getName($setting->specialization_id)}}</td>
                            </tr><tr>
                                <th>Examination Type</th>
                                <td style="text-transform: uppercase;">: {{$setting->type}}</td>
                            </tr><tr>
                                <th>Number of Items</th>
                                <td>: {{$setting->number_of_items}}</td>
                            </tr><tr>
                                <th>Examination Date</th>
                                <td>: {{date("F j, Y, h:i A")}}</td>
                            </tr><tr>
                                <th>Examination Score</th>
                                <td>: {{ $score['score'] }}</td>
                            </tr><tr>
                                <th>Examination Percentage</th>
                                <td>: {{ (($score['score']/$score['number_of_items']) * 100) }}%</td>
                            </tr><tr {{ ((($score['score']/$score['number_of_items']) * 100) >= 70) ? 'class="success"': ' class="danger"'}}>
                                <th>Examination Status</th>
                                <td>: {{ ((($score['score']/$score['number_of_items']) * 100) >= 70) ? '<span class="success">Congratulation! You Passed</span>': '<span class="danger">You need to review more!</span>'}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group pull-right">
                            <a href="{{url('exam/checking/'.csrf_token())}}" id="showScore" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-log-in"></i> Show Checking</a>
                            <a href="{{url('exam/logout')}}" id="logout" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-log-out"></i> Finished</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-view-js')
    @parent
    <script>
        window.history.forward(1);
    </script>
    <SCRIPT TYPE="text/javascript">
        <!--
        function clickIE() {if (document.all) {(message);return false;}}
        function clickNS(e) {if
        (document.layers||(document.getElementById&&!document.all)) {
            if (e.which==2||e.which==3||e.which==8||e.which==44) {(message);return false;}}}
        if (document.layers)
        {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
        else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
        document.oncontextmenu=new Function("return false")
        // -->
    </SCRIPT>
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).keyup(function(e){
                if(e.keyCode == 44){
                    e.preventDefault();
                    return false;
                }
            });
            $(window).keypress(function(e){
                if(e.keyCode != 116){
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@stop