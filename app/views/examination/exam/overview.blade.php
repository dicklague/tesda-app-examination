@extends('layout.master')

@section('page-view-css')
    @parent
    <style>
        #overview{
            margin-top: 40px !important;
        }
        #overview .panel-title{
            text-transform: uppercase;
            font-weight: bolder;
        }
    </style>
@stop

@section('page-view-content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default" id="overview">
                    <div class="panel-heading">
                        <h3 class="panel-title">Examination Overview</h3>
                    </div>
                    <div class="panel-body table-container">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th style="width:180px;">Participant/Examinee</th>
                                <td style="text-transform: uppercase;">: {{$examinee->lastname}} ,{{$examinee->firstname}} {{$examinee->extension_name}} {{$examinee->middlename}}</td>
                            </tr><tr>
                                <th>Field of Specialization</th>
                                <td>: {{Specialization::getName($setting->specialization_id)}}</td>
                            </tr><tr>
                                <th>Examination Type</th>
                                <td style="text-transform: uppercase;">: {{$setting->type}}</td>
                            </tr><tr>
                                <th>Number of Items</th>
                                <td>: {{$setting->number_of_items}}</td>
                            </tr><tr>
                                <th>Duration</th>
                                <td>: {{$setting->duration}}</td>
                            </tr><tr>
                                <th>Examination Date</th>
                                <td>: {{date("F j, Y, h:i A")}}</td>
                            </tr>
                        </table>
                        <div class="well well-sm" style="margin: 10px;">
                            <p style="text-indent:15px;">Read each item carefully. Choose the best answer by clicking on the radio button assigned to it.
                                You can change your answer by clicking on another button which corresponds to your best answer.</p>
                            <p><b>NOTE:</b> You cannot go back to the previous questions when you already proceed to the next question.</p>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <form action="{{url('exam/start')}}" method="post">
                            <div class="btn-group pull-right">
                                <button type="submit" id="startExamBtn" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-log-in"></i> Start Examination</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-view-js')
    @parent
    <script>
        window.history.forward(1);
    </script>
    <SCRIPT TYPE="text/javascript">
        <!--
        function clickIE() {if (document.all) {(message);return false;}}
        function clickNS(e) {if
        (document.layers||(document.getElementById&&!document.all)) {
            if (e.which==2||e.which==3||e.which==8||e.which==44) {(message);return false;}}}
        if (document.layers)
        {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
        else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
        document.oncontextmenu=new Function("return false")
        // -->
    </SCRIPT>
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).keyup(function(e){
                if(e.keyCode == 44){
                    e.preventDefault();
                    return false;
                }
            });
            $(window).keypress(function(e){
                if(e.keyCode != 116){
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@stop