@extends('layout.master')

@section('page-view-css')
    @parent
    <style>
        #authLogin{
            margin-top: -50px !important;
        }
        #authLogin .panel-title{
            text-transform: uppercase;
            font-weight: bolder;
        }
    </style>
@stop

@section('page-view-content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" id="authLogin">
                    <div class="panel-heading">
                        <h3 class="panel-title">Examination Checking </h3>
                    </div>
                    <div class="panel-body table-container">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 5px;">#</th>
                                    <th>Your Answer</th>
                                    <th style="width: 20px;" class="text-center">Checking</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($answer) && count($answer) > 0)
                                @foreach($answer as $key => $value)
                                    <tr {{(Choices::find($value['choice_id'])->answer == 1)? 'class="success" ': 'class="danger"'}}>
                                        <td>{{$key}}</td>
                                        <td>{{Choices::find($value['choice_id'])->option}}</td>
                                        <td class="text-center">{{(Choices::find($value['choice_id'])->answer == 1)? '<i class="fa fa-check"></i>': '<i class="fa fa-times"></i>'}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group pull-right">
                            <a href="{{url('exam/score/'.csrf_token())}}" id="showScore" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-log-in"></i> Show Score</a>
                            <a href="{{url('exam/logout')}}" id="logout" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-log-out"></i> Finished</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-view-js')
    @parent
    <script>
        window.history.forward(1);
    </script>
    <SCRIPT TYPE="text/javascript">
        <!--
        function clickIE() {if (document.all) {(message);return false;}}
        function clickNS(e) {if
        (document.layers||(document.getElementById&&!document.all)) {
            if (e.which==2||e.which==3||e.which==8||e.which==44) {(message);return false;}}}
        if (document.layers)
        {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
        else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
        document.oncontextmenu=new Function("return false")
        // -->
    </SCRIPT>
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).keyup(function(e){
                if(e.keyCode == 44){
                    e.preventDefault();
                    return false;
                }
            });
            $(window).keypress(function(e){
                if(e.keyCode != 116){
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@stop