@extends('layout.master')

@section('page-view-css')
    @parent
    <style>
        #authLogin{
            margin-top: 80px !important;
        }
        #authLogin .panel-title{
            text-transform: uppercase;
            font-weight: bolder;
        }
    </style>
@stop

@section('page-view-content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default" id="authLogin">
                    <div class="panel-heading">
                        <h3 class="panel-title">Authorized Examinee Only</h3>
                    </div>
                    <div class="panel-body">
                        <div id="alert-view"></div>
                        <form action="{{url('exam/login')}}" method="post" accept-charset="UTF-8" role="form" class="form-signin" id="loginForm">
                            {{Form::token()}}
                            <div class="form-group">
                                <label class="control-label">Examinee Code <span>*</span></label>
                                <input class="form-control" type="text" name="examineeId" id="examineeId" value="" autocomplete="Off" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Examinee Password <span>*</span></label>
                                <input class="form-control" type="password" name="examineeCode" id="examineeCode" value="" autocomplete="Off" required/>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group pull-right">
                            <button type="submit" id="loginSubmit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-log-in"></i> Login</button>
                            <button type="reset" id="loginReset" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-repeat"></i> Cancel</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#loginForm").validate({
                errorClass: "error",
                errorElement: "span"
            });

            $(document).on("click","#loginSubmit",function(e){
                e.preventDefault();
                if( $("#loginForm").valid() == true){
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Logging in');
                    $("#loginForm").submit();
                } else {
                    $.each( $("form input.error"), function(){
                        $(this).closest("div").addClass('has-error');
                    });
                }
            });

            $(document).on("click","#loginReset",function(e){
                e.preventDefault();
                $("#loginSubmit").html('<i class="glyphicon glyphicon-log-in"></i> Login');
                $.each( $("input"),function(){
                  $(this).val("");
                  $(this).closest('div').removeClass('has-error').removeClass('has-success');
                });
            });

            @if(Session::has('status'))
                showAlert('error',"{{Session::get('message')}}");
            @endif
        });
    </script>
    <SCRIPT TYPE="text/javascript">
        <!--
        function clickIE() {if (document.all) {(message);return false;}}
        function clickNS(e) {if
        (document.layers||(document.getElementById&&!document.all)) {
            if (e.which==2||e.which==3) {(message);return false;}}}
        if (document.layers)
        {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
        else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
        document.oncontextmenu=new Function("return false")
        // -->
    </SCRIPT>
@stop