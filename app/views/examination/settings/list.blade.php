<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            Examination Settings
        </h3>
    </div>
    <div class="panel-body table-container">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <th>
                    <input type="checkbox" id="checkall" />
                </th>
                <th>Type</th>
                <th>Field</th>
                <th>Items</th>
                <th>Duration</th>
                <th>Date</th>
                <th>isActive</th>
                </thead>
                <tbody id="setting-list-result">
                @if(isset($settings) && count($settings) > 0)
                   @foreach($settings as $record)
                       <tr id="{{$record->id}}">
                           <td>
                               <input value="{{$record->id}}" type="checkbox" class="checkbulk" />
                               <a id="updateRecord" data-record-id="{{$record->id}}" href="{{url('admin/examinations/setting/'.$record->id)}}" data-placement="top" data-toggle="tooltip" title="Update Record"><i class="fa fa-edit"></i> </a>
                               <a id="deleteRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Delete Record"><i class="fa fa-remove"></i> </a>
                           </td>
                           <td style="text-transform: capitalize;">{{ucfirst($record->type)}}</td>
                           <td>{{Specialization::getName($record->specialization_id)}}</td>
                           <td>{{$record->number_of_items}} Items</td>
                           <td>{{$record->duration}}</td>
                           <td>{{date("D M/d/Y",strtotime($record->examination_date))}}</td>
                           <td>{{($record->isActive == 1) ? 'Active': 'No Active'}}</td>
                       </tr>
                   @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>