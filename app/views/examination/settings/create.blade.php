<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Setting Entry</h3>
    </div>
    <div class="panel-body">
        <form id="createSettingForm" action="{{url('admin/examinations/setting')}}" method="post">
            @if( isset($view) && $view == 'question')
            <input type="hidden" name="settingId" id="settingId" value="{{$settings->id}}" />
            @endif
            <div class="form-group">
                <label for="examtype" class="form-label">Type</label>
                <select class="form-control" id="examtype" name="examtype" required>
                    <option value="pre test" @if( (isset($view) && $view == 'question') && $settings->type == "pre test") selected="selected" @endif >PRE Test</option>
                    <option value="post test"  @if( (isset($view) && $view == 'question') && $settings->type == "post test") selected="selected" @endif >POST Test</option>
                </select>
            </div>
            <div class="form-group">
                <label for="field_of_specialization" class="form-label">Field of Specialization</label>
                <select class="form-control" id="field_of_specialization" name="field_of_specialization" required>
                    @if( isset($fields) && count($fields) > 0)
                        @foreach($fields as $key => $value)
                            <option value="{{$value->id}}" @if( (isset($view) && $view == 'question') && $settings->specialization_id == $value->id) selected="selected" @endif >{{Specialization::getName($value->id)}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="number_of_items" class="form-label">Number of Items</label>
                <input type="number" min="1" class="form-control" id="number_of_items" name="number_of_items" required @if( (isset($view) && $view == 'question')) value="{{$settings->number_of_items}}" @else value="" @endif/>
            </div>
            <div class="form-group">
                <label for="examination_date" class="form-label">Examination Date</label>
                <input type="date" min="0" class="form-control" id="examination_date" name="examination_date" required @if( (isset($view) && $view == 'question')) value="{{$settings->examination_date}}" @else value="" @endif/>
            </div>
            <div class="form-group">
                <label for="duration" class="form-label">Duration</label>
                <select id="duration" name="duration" class="form-control" required>
                    @for($x=10;$x<=300;$x=$x+5)
                        <option value="{{$x}} minutes"  @if( (isset($view) && $view == 'question') && $settings->duration == $x.' minutes') selected="selected" @endif >{{$x}} Minutes</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" name="isActive" id="isActive" value="1" @if( (isset($view) && $view == 'question') && $settings->isActive == 1) checked @endif />
                    Active
                </label>
            </div>
        </form>
    </div>
    <div class="panel-footer">
        @if(isset($view) && $view == "question")
        <div class="btn-group pull-right">
            <a href="#" id="createSetting" class="btn btn-sm btn-success">Update Setting</a>
            <a href="#" id="resetSetting" class="btn btn-sm btn-danger">Delete Setting</a>
        </div>
        @else
            <div class="btn-group pull-right">
                <a href="#" id="createSetting" class="btn btn-sm btn-success">Create Setting</a>
                <a href="#" id="resetSetting" class="btn btn-sm btn-danger">Reset Setting</a>
            </div>
        @endif
        <div class="clearfix"></div>
    </div>
</div>
@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#createSettingForm").validate({
                errorClass: 'error',
                errorElement: 'span'
            });
            $(document).on("click","#createSetting",function(e){
                e.preventDefault();
                if( $("#createSettingForm").valid() == true){
                    $("#createSettingForm").submit();
                }
            });
        });
    </script>
@stop