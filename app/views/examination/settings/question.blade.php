<div id="question-select" class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            Examination Settings - Select Questions
        </h3>

        <a id="saveSetting" href="#" data-placement="top" data-toggle="tooltip" title="Save Customized Settings" class="pull-right clickable"><i class="glyphicon glyphicon-save"></i> Save Questions to Setting </a>
    </div>
    <div class="panel-body table-container">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <th>
                    <input type="checkbox" id="checkall" @if(count($setquestion) == count($question)) checked @endif />
                </th>
                <th>Specialization</th>
                <th>Question</th>
                <th>Answer</th>
                </thead>
                <tbody id="question_list_all-result">
                    @if(isset($question) && count($question) > 0)
                        @foreach($question as $record)
                            <tr id="{{$record->id}}">
                                <td>
                                    <input value="{{$record->id}}" type="checkbox" class="checkbulk" @if(in_array($record->id,$setquestion)) checked @endif />
                                </td>
                                <td>{{Specialization::getName($record->specializations_id)}}</td>
                                <td>{{$record->question}}</td>
                                <td>{{$record->answer()}}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $(document).on("click","#saveSetting",function(e){
                e.preventDefault();
                var selectedItems = new Array();
                $('#question-select input.checkbulk:checked').each(function(){
                    selectedItems.push( $(this).val() );
                });

                if(selectedItems.length == 0){
                    showAlert('error','No Questions were selected!');
                } else {
                    $.post("{{url('admin/examinations/setquestion')}}", {
                        id: "{{$settings->id}}",
                        question: selectedItems,
                        _token: "{{csrf_token()}}"
                    }, function (response) {
                        if (response.status == true) {
                            showAlert('success', response.message);
                        } else {
                            showAlert('error', response.message);
                        }
                    });
                }
            });
        });
    </script>