<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{$recordtitle}}</h3>
                <a href="#" data-placement="top" data-toggle="tooltip" title="Create new facilitator's record" class="pull-right clickable"><i data-toggle="modal" data-target="#create_asessor" class="glyphicon glyphicon-plus"></i></a>
                <a gref="#" data-placement="top" data-toggle="tooltip" title="Delete selected record" class="pull-right clickable hide"><i data-toggle="modal" data-target="#deleteRecord"class="glyphicon glyphicon-trash"></i></a>
            </div>
            <div class="panel-body table-container">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>
                                <input type="checkbox" id="checkall" />
                            </th>
                            <th colspan="3" class="text-center">Fullname</th>
                            <th>Field of Specialization</th>
                            <th>Email</th>
                            <th>Contact</th>
                        </thead>
                        <tbody id="facilitators_list-result"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('facilitator.partials.create')
@include('modal.confirmdelete')

@section('page-view-js')
    @parent
    <script>
        $(function(){
            getList();
        });

        function getList(){
            $("#facilitators_list-result").load("{{url('admin/facilitators/list')}}");
            return false;
        }

        $(function(){
            $(document).on("click","#deleteRecord",function(e){
                e.preventDefault();
                var record_id = $(this).attr('data-record-id');
                var con = confirm("Are You sure, you want to delete this record?");
                if(con){
                    $.post("{{url('admin/facilitators/delete')}}/"+record_id,{ _token: "{{csrf_token()}}"},function(response){
                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                        } else {
                            showAlert('error',response.message);
                            $("tr#"+response.data).find("td").css('background',"#cbcbcb")
                        }
                    });
                }
            });

            $(document).on("click","#updateRecord",function(e){

            });
        });
    </script>
@stop
