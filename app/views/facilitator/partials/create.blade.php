<div id="create_asessor" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create new {{$recordtitle}}</h4>
            </div>
            <div class="modal-body">
                <form id="formAssessorCreate" action="#" method="post" accept-charset="UTF-8" role="form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Lastname</label>
                                <input required class="form-control" type="text" name="lastname" id="lastname" value="" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Firstname</label>
                                <input required class="form-control" type="text" name="firstname" id="firstname" value="" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Middlename</label>
                                <input class="form-control" type="text" name="middlename" id="middlename" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Extension Name</label>
                                <input class="form-control" type="text" name="extension_name" id="extension_name" value="" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Sex</label>
                                <select required class="form-control" name="sex" id="sex">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Civilstatus</label>
                                <select required class="form-control" name="civilstatus" id="civilstatus">
                                    <option value="single">Single</option>
                                    <option value="married">Married</option>
                                    <option value="cohabiting">Cohabiting or Live-in</option>
                                    <option value="divorced">Divorced</option>
                                    <option value="widowed">Widowed</option>
                                    <option value="widow_or_widower">Widow or Widower</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Field of Specialization</label>
                                <select class="form-control" id="specialization" name="specialization" required>
                                    @if( isset($fields) && count($fields) > 0)
                                        @foreach($fields as $specialization)
                                            <option value="{{$specialization->id}}">{{FieldCategory::fieldname($specialization->field_category_id) . ' - ' .$specialization->title}}</option>
                                        @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select> </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input required class="form-control" type="email" name="email" id="email" value="" />
                            </div>
                        </div>
                    </div><div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact(mobile/phone)</label>
                                <input class="form-control" type="text" name="contact" id="contact" value="" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnCreateSubmit" type="button" class="btn btn-success"><i class="fa fa-save"></i> Create</button>
                <button id="btnCreateReset" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#formAssessorCreate").validate({
                errorClass: 'error',
                errorElement: 'span'
            });

            $(document).on("click","#btnCreateSubmit",function(e){
                e.preventDefault();
                if( $("#formAssessorCreate").valid() == true){
                    var data = {
                        'firstname': $("#firstname").val().trim(),
                        'lastname': $("#lastname").val().trim(),
                        'middlename': $("#middlename").val().trim(),
                        'extension_name': $("#extension_name").val().trim(),
                        'gender': $("#sex").val().trim(),
                        'civilstatus': $("#civilstatus").val().trim(),
                        'field': $("#specialization").val().trim(),
                        'email': $("#email").val().trim(),
                        'contact': $("#contact").val().trim(),
                        '_token': '{{csrf_token()}}'
                    };

                    $.post("{{url('admin/facilitators/create')}}",data,function(response){
                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                            $.each( $("#formAssessorCreate input"),function(){
                                $(this).val("");
                            });
                            $('#create_asessor').modal('hide');
                        } else {
                            showAlert('error',response.message);
                        }
                    });
                }
            });

            $(document).on("click","#btnCreateReset",function(e){
                resetForm();
            });

            function resetForm(){
                $("#formAssessorCreate").find("input").val('');
               // $("#formAssessorCreate").find("email").val('');
                $("#formAssessorCreate").find("select").val('');

                $("#formAssessorCreate").find("input").removeClass('error');
                $("#formAssessorCreate").find("select").removeClass('error');
                $("#formAssessorCreate").find("span.error").hide();
            }
        });
    </script>
@stop
