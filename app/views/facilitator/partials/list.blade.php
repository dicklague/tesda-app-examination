@if( isset($list) && count($list) )
    @foreach($list as $record)
        <tr id="{{$record->id}}">
            <td>
                <input value="{{$record->id}}" type="checkbox" class="checkbulk" />
                <a class="hide" id="updateRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Update Record"><i class="fa fa-edit"></i> </a>
                <a class="hide"  id="deleteRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Delete Record"><i class="fa fa-remove"></i> </a>
            </td>
            <td>{{$record->lastname}}</td>
            <td>,{{$record->firstname}} {{$record->extensio_name}}</td>
            <td>{{$record->middlename}}</td>
            <td>{{$record->specialization->first()->specialization()->first()->getSpecialization()}}</td>
            <td>{{$record->email}}</td>
            <td>{{$record->contact_number_primary}}</td>
        </tr>
    @endforeach
@endif
