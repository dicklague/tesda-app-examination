<div id="register_examinee" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Register new {{$recordtitle}}</h4>
            </div>
            <div class="modal-body">
                <form id="formRegisterExamineeCreate" action="#" method="post" accept-charset="UTF-8" role="form">
                    <input type="hidden" name="examineeId" id="examineeId" value="" />
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Lastname</label>
                                <input type="text" name="lastname" id="lastname" class="form-control" required />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Firstname</label>
                                <input type="text" name="firstname" id="firstname" class="form-control" required />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Middlename</label>
                                <input type="text" name="middlename" id="middlename" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Extension</label>
                                <input type="text" name="extension_name" id="extension_name" class="form-control" />
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label class="form-label">Sex</label>
                            <select class="form-control" id="sex" name="sex" required>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Field of Specialization</label>
                            <select class="form-control" id="specialization" name="specialization" required>
                                @if( isset($fields) && count($fields) > 0)
                                    @foreach($fields as $specialization)
                                        <option value="{{$specialization->id}}">{{FieldCategory::fieldname($specialization->field_category_id) . ' - ' .$specialization->title}}</option>
                                    @endforeach
                                @else
                                    <option value=""></option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Contact Number</label>
                                <input type="text" name="contactnumber" id="contactnumber" class="form-control" />
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">School</label>
                                <textarea class="form-control" id="school" name="school" required></textarea>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button id="btnCreateSubmit" type="button" class="btn btn-success"><i class="fa fa-save"></i> Create</button>
                <button id="btnCreateReset" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#formRegisterExamineeCreate").validate({
                errorClass: 'error',
                errorElement: 'span'
            });

            $(document).on("click","#btnCreateSubmit",function(e){
                if( $("#formRegisterExamineeCreate").valid() == true){
                    var request = {
                        'id': $("#examineeId").val().trim(),
                        'firstname': $("#firstname").val().trim(),
                        'lastname': $("#lastname").val().trim(),
                        'middlename': $("#middlename").val().trim(),
                        'extension_name': $("#extension_name").val().trim(),
                        'sex': $("#sex").val().trim(),
                        'specialization': $("#specialization").val().trim(),
                        'school': $("#school").val().trim(),
                        'contact_number': $("#contactnumber").val().trim(),
                        '_token': '{{csrf_token()}}'
                    };

                    $.post("{{url('admin/examinees/create')}}",request,function(response){
                        //console.log(response);

                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                            compareList();
                            $.each( $("#formRegisterExamineeCreate input"),function(){
                                $(this).val("");
                            });
                            $('#register_examinee').modal('hide');
                        } else {
                            showAlert('error',response.message);
                        }

                    });
                } else {

                }
            });

            $(document).on("click","#btnCreateReset",function(e){
                $("#formRegisterExamineeCreate").find("input").removeClass('error');
                $("#formRegisterExamineeCreate").find("select").removeClass('error');
                $("#formRegisterExamineeCreate").find("textarea").removeClass('error');
                $("#formRegisterExamineeCreate").find("span.error").remove();
            });
        });
    </script>
@stop