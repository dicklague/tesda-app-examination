@if( count($list) )
    @foreach($list as $record)
        @if($record->specialization->first() !== null)
            <tr id="{{$record->id}}" record-id="{{$record->id}}">
                <td>
                    <input value="{{$record->id}}" type="checkbox" class="checkbulk" />
                    <a id="updateRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Update Record"><i class="fa fa-edit"></i> </a>
                    <a id="removeRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Delete Record"><i class="fa fa-remove"></i> </a>
                </td>
                <td>{{strtoupper($record->lastname)}}</td>
                <td>,{{strtoupper($record->firstname)}} {{strtoupper($record->extension_name)}}</td>
                <td>{{strtoupper($record->middlename)}}</td>
                <td>{{strtoupper(substr($record->gender,0,1))}}</td>
                <td>{{($record->specialization->first()->specialization() !== null)? $record->specialization->first()->specialization()->first()->getSpecialization(): ''}}</td>
                <td>{{$record->school}}</td>
                <td>{{$record->contact_number}}</td>
            </tr>
        @endif
    @endforeach
@endif  
