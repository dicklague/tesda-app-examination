<div id="select_examinee" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select from Old {{$recordtitle}}</h4>
            </div>
            <div class="modal-body">
                <form id="frmSelectedExaminee">
                    <div class="options">
                        <label>
                            <input type="checkbox" id="selAllExaminee" /> Select/Unselect All Examinee
                        </label>
                    </div>
                    <div class="list-examinee-old">
                        @if( count($examinee_record) )
                            @foreach($examinee_record as $record)
                                <div id="recid_{{$record->id}}" class="record" record-id = "{{$record->id}}">
                                    <span><input type="checkbox" class="listExaminee" id="examinee_{{$record->id}}" name="selExaminee[]" value="{{$record->id}}"/> </span>
                                    <span>{{$record->lastname}}</span>
                                    <span>, {{$record->firstname}}</span>
                                    <span>- ({{$record->school}})</span>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="form-label">Field of Specialization</label>
                            <select class="form-control" id="fld" name="fld" required>
                                @if( isset($fields) && count($fields) > 0)
                                    @foreach($fields as $specialization)
                                        <option value="{{$specialization->id}}">{{FieldCategory::fieldname($specialization->field_category_id) . ' - ' .$specialization->title}}</option>
                                    @endforeach
                                @else
                                    <option value=""></option>
                                @endif
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSelectCreateSubmit" type="button" class="btn btn-success"><i class="fa fa-save"></i> Save Selected</button>
                <button id="btnSelectCreateReset" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('page-content-view-css')
    @parent
    <style>
    .list-examinee-old{
        min-height: 300px;
        max-height: 300px;
        height: 300px;
        overflow: scroll;
    }
    .list-examinee-old .record{
        padding: 8px;
        border-bottom: 1px solid #000033;
        text-transform: uppercase;
    }
    .list-examinee-old .record:last-child{
        border-bottom: none medium transparent;
    }
    .list-examinee-old .record:hover{
        background: #c0c0c0;
        cursor: pointer;
    }
    .list-examinee-old .selected{
        background: #66d9ef;
    }
    .options label:hover{
        cursor: pointer;
    }
    </style>
@stop

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#frmSelectedExaminee").validate({
                errorClass: 'error',
                errorElement: 'span'
            });

            $(".list-examinee-old .record").on("click",function(e){
                var checkbox = $(this).find("input[type='checkbox']");
                var id = checkbox.val();

                if(checkbox.is(':checked') == true){
                    checkbox.prop('checked',false);
                } else {
                    checkbox.prop('checked',true);
                }
                selectedList();
            });

            $("#selAllExaminee").on("change",function(){
                var isChecked = $(this).is(":checked");
                if(isChecked == true){
                    $(".list-examinee-old .record").each(function(){
                        var checkbox = $(this).find("input[type='checkbox']");
                        checkbox.prop('checked',true);
                    });
                } else{
                    $(".list-examinee-old .record").each(function(){
                        var checkbox = $(this).find("input[type='checkbox']");
                        checkbox.prop('checked',false);
                    });
                }
                selectedList();
            });

            $("#btnSelectCreateSubmit").on("click",function(e){
                e.preventDefault();
                e.stopPropagation();

                if( $("#frmSelectedExaminee").valid() == true){
                    var selectedItem = new Array();

                    $(".list-examinee-old .record input[type='checkbox']:checked").each(function(){
                        selectedItem.push( $(this).val() );
                    });
                    if(selectedItem.length === 0){
                        showAlert("error","No Examinees have been selected.");
                    } else {
                        var data = {
                            field: $("#fld option:selected").val(),
                            id: selectedItem.toString(),
                            _token: "{{csrf_token()}}"
                        };

                        $.ajax({
                            url: '{{url('admin/examinees/registerexaminee')}}',
                            data: data,
                            type: 'POST'
                        })
                        .done(function(response){
                            showAlert('success',response.message);

                            $("#select_examinee").modal('hide');

                            getList();
                            compareList();
                        })
                        .fail(function(error){
                            showAlert('error',error.message);
                        });
                    }
                } else {
                    showAlert('error','Please Correct Form Errors.');
                }
            });

            var selectedList = function(){
                $(".list-examinee-old .record").each(function(){
                    var checkbox = $(this).find("input[type='checkbox']");
                    var id = checkbox.val();

                    if(checkbox.is(':checked') == true){
                        $(this).addClass('selected');
                    } else {
                        $(this).removeClass('selected');
                    }
                });
            };
        });
    </script>
@stop