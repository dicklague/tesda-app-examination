<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{$recordtitle}}/Participants</h3>
                <a href="#" id="unregisterExaminee" data-placement="top" data-toggle="tooltip" title="Delete selected Examinee " class="pull-right clickable"><i class="glyphicon glyphicon-trash"></i> Remove Selected Participants</a>
                <a href="#" id="selectRegisterExaminee" data-placement="top" data-toggle="tooltip" title="Create Examinee from Last record" class="pull-right clickable"><i class="glyphicon glyphicon-plus"></i> Select From Existing Examinees</a>
                <a href="#" id="createRegisterExaminee" data-placement="top" data-toggle="tooltip" title="Create new Examinee record" class="pull-right clickable"><i class="glyphicon glyphicon-plus"></i> Add New</a>
            </div>
            <div class="panel-body table-container">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
	                        <th>
	                            <input type="checkbox" id="checkall" />
	                        </th>
	                        <th>Lastname</th>
	                        <th>Firstname</th>
	                        <th>Middlename</th>
	                        <th>Sex</th>
	                        <th>Field of Specialization</th>
	                        <th>School</th>
	                        <th>Contact Number</th>
                        </thead>
                        <tbody id="field-list-result"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('examinee.partials.create')
@include('examinee.partials.select')
@include('modal.confirmdelete')

@section('page-view-js')
    @parent
    <script>
        $(function(){
            getList();
            compareList();
        });

        function getList(){
            $("#field-list-result").load("{{url('admin/examinees/list')}}");
            return false;
        }

        function compareList(){
            $("#field-list-result tr").each(function(){
                var id = $(this).attr('record-id');

                $(".list-examinee-old .record").each(function(){
                    var idd = $(this).attr('record-id');
                    if(id == idd){
                        $(this).remove();
                    }
                });
            });
        }

        $(function(){
            $(document).on("click","#removeRecord",function(e){
                e.preventDefault();
                e.stopPropagation();

                var record_id = $(this).attr('data-record-id');
                var con = confirm("Are You sure, you want to delete this record?");
                if(con){
                    $.post("{{url('admin/examinees/unregister')}}",{ id: record_id, _token: "{{csrf_token()}}"}, function(response){
                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                            window.location.reload();
                        } else {
                            showAlert('error',response.message);
                            $("tr#"+response.data).find("td").css('background',"#cbcbcb")
                        }
                    });
                }
            });

            $(document).on("click","#unregisterExaminee",function(e){
                e.preventDefault();
                e.stopPropagation();

                var selectedItems = new Array();
                $('#field-list-result input.checkbulk:checked').each(function(){
                    selectedItems.push( $(this).val() );
                });

                if(selectedItems.length == 0){
                    showAlert('error','No Participants were selected!');
                } else {
                    var con = confirm("Are You sure, you want to remove the selected Participants?");
                    if(con) {
                        $.post("{{url('admin/examinees/unregister')}}", {
                            id: selectedItems.toString(),
                            _token: "{{csrf_token()}}"
                        }, function (response) {
                            if (response.status == true) {
                                showAlert('success', response.message);
                                getList();
                                window.location.reload();
                            } else {
                                showAlert('error', response.message);
                            }
                        });
                    }
                }
            });

            $(document).on("click","#createRegisterExaminee",function(e){
                e.preventDefault();
                $("#examineeId").val("");
                $("#lastname").val("");
                $("#firstname").val("");
                $("#middlename").val("");
                $("#extension_name").val("");
                $("#sex").val("");
                $("#specialization").val("");
                $("#contactnumber").val("");
                $("#school").val("");

                $("#register_examinee").find(".modal-title").html("Register new Examinee");
                $("#register_examinee").modal('show');
            });

            $(document).on("click","#selectRegisterExaminee",function(e){
                compareList();
                $("#select_examinee").find(".modal-title").html("Select From Previous Examinee");
                $("#select_examinee").modal('show');
            });

            $(document).on("click","#updateRecord",function(e){
                e.preventDefault();
                var id = $(this).attr('data-record-id');
                $.post("{{url('admin/examinees/record')}}",{ id: id, _token: '{{csrf_token()}}'},function(response){
                    if(response.status == true) {
                        $("#examineeId").val(id);
                        $("#lastname").val(response.data.lastname);
                        $("#firstname").val(response.data.firstname);
                        $("#middlename").val(response.data.middlename);
                        $("#extension_name").val(response.data.extension_name);
                        $("#sex").val(response.data.gender);
                        $("#specialization").val(response.data.specialization[0].specializations_id);
                        $("#contactnumber").val(response.data.contact_number);
                        $("#school").val(response.data.school);

                        $("#register_examinee").find(".modal-title").html("Update Examinee Record");
                        $("#register_examinee").modal('show');
                    } else {
                        showAlert('error',response.message);
                    }
                });
            });
        });
    </script>
@stop  
