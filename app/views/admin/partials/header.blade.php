<?php $profile = Facilitator::where('users_id','=', Sentry::getUser()->id)->first(); ?>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">EXAMINATION</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{url('admin')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                <li><a href="{{url('admin/facilitators')}}"><i class="fa fa-users"></i> Facilitator</a></li>
                <li><a href="{{url('admin/examinees')}}"><i class="fa fa-users"></i> Examinee</a></li>
                <li><a href="{{url('admin/fields')}}"><i class="fa fa-cog"></i> Fields/Specialization</a></li>
                <li><a href="{{url('admin/questions')}}"><i class="fa fa-question-circle"></i> Question Bank</a></li>
                <li><a href="{{url('admin/examinations')}}"><i class="fa fa-cogs"></i> Examination Setting</a></li>
                <li><a href="{{url('admin/monitoring')}}"><i class="fa fa-laptop"></i> Monitoring</a></li>
                <li><a href="{{url('admin/setup')}}"><i class="fa fa-archive"></i> Setup Event</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Account
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="navbar-content">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="http://placehold.it/120x120"
                                             alt="Alternate Text" class="img-responsive" />
                                        <p class="text-center small">
                                            <a href="#">Change Photo</a></p>
                                    </div>
                                    <div class="col-md-7">
                                        <span> @if(count($profile) > 0) {{$profile->firstname }} {{$profile->lastname}} @else Administrator @endif </span>
                                        <p class="text-muted small">
                                            @if( count($profile) > 0 ) {{$profile->email}} @else {{Sentry::getUser()->email}} @endif
                                        </p>
                                        <div class="divider">
                                        </div>
                                        <a href="#" class="btn btn-primary btn-sm active">View Profile</a>
                                    </div>
                                </div>
                            </div>
                            <div class="navbar-footer">
                                <div class="navbar-footer-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" class="btn btn-default btn-sm">Change Passowrd</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{url('admin/logout')}}" class="btn btn-default btn-sm pull-right">Sign Out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
