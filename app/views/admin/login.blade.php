@extends('layout.master')

@section('page-view-css')
    @parent
    <style>
        #authLogin{
            margin-top: 80px !important;
        }
        #authLogin .panel-title{
            text-transform: uppercase;
            font-weight: bolder;
        }
    </style>
@stop

@section('page-view-content')
    <div id="alert-view" class="col-md-4 col-md-offset-4 hide">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong class="alert-status">Success!</strong> <span class="alert-message">Record has been successfully added.</span>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default" id="authLogin">
                    <div class="panel-heading">
                        <h3 class="panel-title">Administrator Login</h3>
                    </div>
                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form" class="form-signin" id="loginForm">
                            <div class="form-group">
                                <label class="control-label">Email <span>*</span></label>
                                <input class="form-control" type="email" name="email" id="email" value="" autocomplete="Off" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password <span>*</span></label>
                                <input class="form-control" type="password" name="password" id="password" value="" autocomplete="Off" required/>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group pull-right">
                            <button type="submit" id="loginSubmit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-log-in"></i> Login</button>
                            <button type="reset" id="loginReset" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-repeat"></i> Cancel</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-view-js')
    @parent
    <script src="{{url('assets/plugins/jquery-validation-1.13.1/dist/jquery.validate.min.js')}}"></Script>
    <script>
        $(function(){
            $("#loginForm").validate({
                errorClass: "error",
                errorElement: "span"
            });

            $(document).on("click","#loginSubmit",function(e){
                e.preventDefault();
                if( $("#loginForm").valid() == true){
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Logging in');

                    var credentials = {
                      email: $("#email").val().trim(),
                      password: $("#password").val().trim(),
                      _token: "{{csrf_token()}}"
                    };

                    $.post("{{url('admin/login')}}",credentials,function(response){
                        if(response.status == false){
                            $("#alert-view strong.alert-status").html("Error!");
                            $("#alert-view span.alert-message").html(response.message);
                            $("#alert-view").find('div.alert').removeClass('alert-success').addClass('alert-danger');
                            $("#alert-view").removeClass('hide');
                        } else {
                            if( response.status == true){
                                $("#alert-view strong.alert-status").html("Success!");
                                $("#alert-view span.alert-message").html("Redirecting to Adminstrator's Dashboard.");
                                $("#alert-view").find('div.alert').removeClass('alert-error').addClass('alert-success');
                                $("#alert-view").removeClass('hide');

                                window.location = "{{url('admin')}}";
                            }
                        }
                    });
                } else {
                    $.each( $("form input.error"), function(){
                        $(this).closest("div").addClass('has-error');
                    });
                }
            });

            $(document).on("click","#loginReset",function(e){
                e.preventDefault();
                $("#loginSubmit").html('<i class="glyphicon glyphicon-log-in"></i> Login');
                $("#loginForm span.error").hide();
                $.each( $("input"),function(){
                    $(this).val("");
                    $(this).closest('div').removeClass('has-error').removeClass('has-success');
                });
            });
        });
    </script>
@stop