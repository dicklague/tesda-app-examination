@extends('layout.master')

@section('page-view-css')
    @parent

@stop

@section('page-view-content')
    <div id="wrapper-view" class="row-fluid">
        <div id="header-view">
            @include('admin.partials.header')
        </div>

        @if(Session::has('alert_status'))
            <div id="alert-view" class="container-fluid">
                <div class="alert {{Session::get('alert_status_view')}} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{Session::get('alert_status_message')}}
                </div>
            </div>
        @else
            <div id="alert-view" class="container-fluid hide"></div>
        @endif

        <div id="content-view" class="container-fluid">
            @if(isset($pagecontent))
                @include($pagecontent)
            @else
                @include('admin.partials.content')
            @endif
        </div>
    </div>
    <div id="footer-view">
        @include('admin.partials.footer')
    </div>
@stop

@section('page-view-js')
    @parent

@stop