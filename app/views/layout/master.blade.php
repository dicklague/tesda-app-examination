<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}} | DIVISION SKILLS ENHANCEMENT TRAINING</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta content="" name="description" />
    <meta content="Dick De Luna Lague" name="author" />

    <!-- Bootstrap -->
    <link href="{{asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap/dist/css/bootstrap-theme.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('assets/plugins/fonts/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Bootstrap DatePicker CSS -->
    <link href="{{asset('assets/plugins/datepicker/css/datepicker.css')}}" rel="stylesheet">
    <!-- Global Master Custom css -->
    <link href="{{asset('assets/css/master.css')}}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('page-view-css')
    @yield('page-content-view-css')
</head>
<body>
@yield('page-view-content')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('assets/plugins/jquery-1.11.2.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('assets/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-validation-1.13.1/dist/jquery.validate.min.js')}}"></Script>
<script src="{{url('assets/plugins/datepicker/js/bootstrap-datepicker.js')}}"></Script>
<!-- Global Custom Javascript -->
<script src="{{asset('assets/js/global.js')}}"></script>
@yield('page-view-js')
@yield('page-conetent-view-js')
</body>
</html>