@if(count($list) > 0)
    @foreach($list as $record)
        <tr>
            <?php extract(Examinee::name($record->examinee_id,'array')); ?>
            <td>{{$lastname}}</td>
            <td>{{$firstname}}</td>
            <td>{{'[ '.$record->score.'/'.$record->number_of_items.' ] ( '.(($record->score/$record->number_of_items)*100).'% )'}}</td>
            <td>Passed</td>
        </tr>
    @endforeach
@endif