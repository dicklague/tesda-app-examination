@if(count($field) > 0)
    <?php $count = 1; ?>
    <?php $total_count = (count($field) > 3) ? floor(count($field)/3): 1; ?>
    @foreach($field as $record)
        @if($count == 1)
            <div class="col-md-4">
        @endif
            <div class="panel panel-default" id="record{{$record->id}}">
                <div class="panel-heading">
                    <h3 class="panel-title">{{Specialization::getname($record->specialization->id)}}</h3>
                    <a href="#" data-id="{{$record->id}}" data-placement="top" data-toggle="tooltip" title="Refresh Listing" class="refreshList pull-right clickable"><i class="glyphicon glyphicon-refresh"></i></a>
                    <a href="#" data-id="{{$record->id}}" data-placement="top" data-toggle="tooltip" title="Print Listing" class="printList pull-right clickable"><i class="glyphicon glyphicon-print"></i></a>
                </div>
                <div class="panel-body table-container">
                    <div class="table-responsive">
                        <table class="table table-hover table-striped" id="listRecord{{$record->id}}"></table>
                    </div>
                </div>
                <div class="panel-footer">Total Passers: <span id="totalPassers{{$record->id}}"></span></div>
            </div>

        @if(count($field) > 1 && $count%$total_count == 0)
            </div>
            @if($count <> count($field))
                <div class="col-md-4">
            @endif
        @endif
        <?php $count++; ?>
    @endforeach
@endif

@section('page-view-js')
    @parent
    <script>
        $(function(){
            @if(count($field) > 0)
                @foreach($field as $record)
                    getResult( "{{$record->id}}");
                @endforeach
            @endif

            $(document).on("click","a.refreshList",function(e){
                e.preventDefault();
                e.stopPropagation();

                var id = $(this).attr('data-id');
                getResult(id);
            });

            $(document).on("click","a.printList",function(e){
                e.preventDefault();
                e.stopPropagation();

                var id = $(this).attr('data-id');

                if(id == null || id == ''){
                    showAlert("error","Please Specify what you want to print.");
                } else {
                    var url = "{{url('admin/print')}}?id=" + id +"&_token={{csrf_token()}}";
                    $("iframe#printPage").remove();
                    $('<iframe id="iframeprint" name="printPage" src=' + url + ' style="display: none; @media print { display: block; }"></iframe>').insertAfter('body');
                    callPrint('iframeprint');
                }
            });
        });

        function getResult(id){
            $("#record"+id).find('a.refreshList').addClass('fa-spin');
            var url = "{{url('admin/result')}}?id="+id+"&_token={{csrf_token()}}";
            $("#listRecord"+id).load(url);

            setTimeout(function(){
                var rows= $("#listRecord"+id+" tr").length;
                $("#totalPassers"+id).html(rows);
            },5e3);

            setTimeout(function(){
                $("#record"+id).find('a.refreshList').removeClass('fa-spin');
            },3e3);
        }


        //initiates print once content has been loaded into iframe
        function callPrint(iframeId) {
            var PDF = document.getElementById(iframeId);
            PDF.focus();
            PDF.contentWindow.print();
        }
    </script>
@stop