<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}}</title>
    <meta content="" name="description" />
    <meta content="Dick De Luna Lague" name="author" />

    <style>
        html {
            font-family: sans-serif;
            font-size: 12px;
        }
        body {
            margin: 0;
        }
        h3{
            margin: 0px;
        }
        .table{
            width: 100%;
        }
        .text-code{
            font-family: 'Courier New', Monospace;
            font-weight: bolder;
            font-size: 16px;
            letter-spacing: 2px;
        }
        .text-center{
            text-align: center;
        }
        @media print {
            .row-print{
                page-break-inside: avoid;
            }
        }
    </style>
</head>
<body>
@if(count($list) > 0)
    <h3>Examination Passers - {{$specialization}} - {{ucwords($test)}}</h3>
    <p>({{date('F/d/Y h:i A')}})<br /></p>

    <table class="table" border="1" cellpadding="5" cellspacing="0">
        <thead>
        <tr>
            <th scope="col" nowrap="nowrap">Participant/Examinee</th>
            <th scope="col" nowrap="nowrap">Field of Specialization</th>
            <th scope="col" nowrap="nowrap" class="text-center">Score</th>
            <th scope="col" nowrap="nowrap" class="text-center">Percentage</th>
            <th scope="col" nowrap="nowrap" class="text-center">Remarks</th>
        </tr>
        </thead>
        <tbody>
            @foreach($list as $record)
                <tr class="row-print">
                    <td>{{Examinee::name($record->examinee_id,'string')}}</td>
                    <td>{{$specialization}}</td>
                    <td style="text-align: center;">{{$record->score.' / '.$record->number_of_items}}</td>
                    <td style="text-align: center;">{{'( '.(($record->score/$record->number_of_items)*100).'% )'}}</td>
                    <td style="text-align: center;">Passed</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
</body>
</html>