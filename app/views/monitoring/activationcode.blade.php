<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}}</title>
    <meta content="" name="description" />
    <meta content="Dick De Luna Lague" name="author" />

    <style>
        html {
            font-family: sans-serif;
            font-size: 12px;
        }
        body {
            margin: 0;
        }
        h3{
            margin: 0px;
        }
        .table{
            width: 100%;
        }
        .text-code{
            font-family: 'Courier New', Monospace;
            font-weight: bolder;
            font-size: 16px;
            letter-spacing: 2px;
        }
        .text-center{
            text-align: center;
        }
    </style>
</head>
<body>
    @if(isset($activation) && count($activation) > 0)
        <h3>Activation Code - {{$specialization}} - {{$test}}</h3>
        <table class="table" border="1" cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col" nowrap="nowrap">Participant/Examinee</th>
                    <th scope="col" nowrap="nowrap">Field of Specialization</th>
                    <th scope="col" nowrap="nowrap" class="text-center">User Code</th>
                    <th scope="col" nowrap="nowrap" class="text-center">Pass Code</th>
                </tr>
            </thead>
            <tbody>
            @foreach($activation as $record)
                <tr>
                   <td nowrap="nowrap" style="text-transform: uppercase;">{{$record->examinee()->first()->lastname}} ,{{$record->examinee()->first()->firstname}} {{$record->examinee()->first()->extension_name}} {{substr($record->examinee()->first()->middlename,0,1)}}</td>
                   <td>{{ucfirst($record->examinationsetting()->first()->type)}} - {{Specialization::getName($record->examinationsetting()->first()->specialization_id)}}</td>
                   <td class="text-center text-code">{{$record->username}}</td>
                   <td class="text-center text-code">{{$record->password}}</td>
               </tr>
            @endforeach
            </tbody>
        </table>
    @endif

</body>
</html>