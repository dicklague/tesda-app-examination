@section('page-view-css')
    @parent
    <style>
        .panel-list .panel-body { padding:0px; }
        .panel-list .panel-footer .pagination { margin: 0; }
        .panel-list .glyphicon,
        .panel-list .list-group-item .glyphicon { margin-right:5px; }
        .panel-list .panel-body .radio,
        .panel-list .panel-body .checkbox { display:inline-block;margin:0px; }
        .panel-list .panel-body input[type=checkbox]:checked + label { text-decoration: line-through;color: rgb(128, 144, 160); }
        .panel-list .list-group-item:hover,
        .panel-list .list-group-item:focus {text-decoration: none;background-color: rgb(245, 245, 245);}
        .panel-list .list-group { margin-bottom:0px; }
        .panel-list .list-group { border-radius: 0px; -moz-border-radius: 0px; -o-border-radius: 0px; -webkit-border-radius: 0px; }
        .panel-list .list-group .list-group-item { border-left: none medium transparent; border-right: none medium transparent; }
        .panel-list .list-group .list-group-item:last-child,
        .panel-list .list-group .list-group-item:first-child{ border-radius: 0px; -moz-border-radius: 0px; -o-border-radius: 0px; -webkit-border-radius: 0px; }
        .panel-list .list-group .list-group-item:first-child{ border-top: none medium transparent;}
        .panel-list .list-group .list-group-item:last-child{ border-bottom: none medium transparent;}
    </style>
@stop
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default panel-list">
            <div class="panel-heading">
                <h3 class="panel-title">Examination</h3>
            </div>
            <div class="panel-body table-container">
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active"><a href="#pretest" aria-controls="pretest" role="tab" data-toggle="tab">PRE TEST</a></li>
                    <li role="presentation"><a href="#posttes" aria-controls="posttest" role="tab" data-toggle="tab">POST TEST</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pretest">
                        <div class="list-group">
                            @foreach($pre_settings as $pre_settings_record)
                                <a href="" class="list-group-item pretest-activation-list" data-item-id="{{$pre_settings_record->id}}">
                                   {{Specialization::getName($pre_settings_record->specialization_id)}}
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="posttes">
                        <div class="list-group">
                            @foreach($post_settings as $post_settings_record)
                                <a href="" class="list-group-item posttest-activation-list" data-item-id="{{$post_settings_record->id}}">
                                   {{Specialization::getName($post_settings_record->specialization_id)}}
                                </a>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Examinee - Activation</h3>
                <a id="activateExaminee"href="" data-placement="top" data-toggle="tooltip" title="ACtivate Selected Examinee" class="pull-right clickable"><i class="fa fa-check-square-o"></i> Activate Selected Examinee</a>
                <a id="printActivationCode" data-examId="" href="" data-placement="top" data-toggle="tooltip" title="Print Activation Code" class="pull-right clickable"><i class="fa fa-print"></i> Print Activation Code</a>
            </div>
            <div class="panel-body table-container">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <th>
                            <input type="checkbox" id="checkall" />
                        </th>
                        <th colspan="3">Examinee</th>
                        <th>Field of Specialization</th>
                        <th>Username</th>
                        <th>Activation</th>
                        <th>Status</th>
                        </thead>
                        <tbody id="activation-list-result"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $(document).on("click","a.pretest-activation-list",function(e){
                e.preventDefault();
                var id = $(this).attr("data-item-id");
                $("a.pretest-activation-list").removeClass('active');
                $("a.posttest-activation-list").removeClass('active');
                $(this).addClass('active');
                getExamineeActivationList(id);
            });

            $(document).on("click","a.posttest-activation-list",function(e){
                e.preventDefault();
                var id = $(this).attr("data-item-id");
                $("a.pretest-activation-list").removeClass('active');
                $("a.posttest-activation-list").removeClass('active');
                $(this).addClass('active');
                getExamineeActivationList(id);
            });

            function getExamineeActivationList(id){
                $("#printActivationCode").attr("data-examId",id);
                $.get("{{url('admin/monitoring/examinee')}}",{ id: id, _token: '{{csrf_token()}}' },function(response){
                    $("#activation-list-result").html(response);
                });
            }

            $(document).on("click","#activateExaminee",function(e){
                e.preventDefault();
                var selectedItems = new Array();
                $('#activation-list-result input.checkbulk:checked').each(function(){
                    var data = {
                        id: $(this).val(),
                        username: $(this).attr('data-username'),
                        password: $(this).attr('data-password'),
                        examinationId: $(this).attr('data-examination-id')
                    };
                    selectedItems.push(data);
                });

                if(selectedItems.length == 0){
                    showAlert('error','No Examinee were selected!');
                } else {
                    $.post("{{url('admin/monitoring/activate')}}", {
                        activation: selectedItems,
                        _token: "{{csrf_token()}}"
                    }, function (response) {
                        if (response.status == true) {
                            showAlert('success', response.message);
                            $("#activation-list-result").html(response.data);
                            $("input[type='checkbox']").prop('checked',false);
                        } else {
                            showAlert('error', response.message);
                        }
                    });
                }
            });

            $(document).on("click","#printActivationCode",function(e){
                e.preventDefault();
                var id = $(this).attr('data-examId');
                if(id == null || id == ''){
                    showAlert("error","No Examination has been selected");
                } else {
                    var url = "{{url('admin/monitoring/activation')}}?id=" + id +"&_token={{csrf_token()}}";
                    $("iframe#printPage").remove();
                    $('<iframe id="iframeprint" name="printPage" src=' + url + ' style="display: none; @media print { display: block; }"></iframe>').insertAfter('body');
                    callPrint('iframeprint');
                }
            });

            //initiates print once content has been loaded into iframe
            function callPrint(iframeId) {
                var PDF = document.getElementById(iframeId);
                PDF.focus();
                PDF.contentWindow.print();
            }
        });
    </script>
@stop