<div class="row">
    <div class="col-md-8 col-xs-12 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{$recordtitle}}</h3>
                <a href="#" id="createExaminationSetup" data-placement="top" data-toggle="tooltip" title="Setup New Examination" class="pull-right clickable"><i class="glyphicon glyphicon-plus"></i></a>
                <a href="#" data-placement="top" data-toggle="tooltip" title="Delete selected Examoination Setup" class="pull-right clickable hide"><i data-toggle="modal" data-target="#deleteRecord"class="glyphicon glyphicon-trash"></i></a>
            </div>
            <div class="panel-body table-container">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <th>
                            <input type="checkbox" id="checkall" />
                        </th>
                        <th>Setup</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Participants</th>
                        </thead>
                        <tbody id="setup-list-result"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('setup.partials.form')

@section('page-view-js')
    @parent
    <script>
        function getList(){
            $("#setup-list-result").load("{{url('admin/setup/list')}}");
            return false;
        }

        $(function(){
            getList();

            $("#createExaminationSetup").on("click",function(e){
                e.preventDefault();
                e.stopPropagation();

                $("#formExaminationSetupCreate").find("#examination_title").val("").focus();
                $("#formExaminationSetupCreate").find("#activate").prop('checked',false);

                $("#setup_examination_event").find(".modal-title").html("Setup New Examination");
                $("#setup_examination_event").modal('show');
            });

            $(document).on("click","a.btnActivateSetup",function(e){
                e.preventDefault();
                e.stopPropagation();

                $(this).removeClass('btn-default').addClass('btn-success');
                $(this).html('<i class="fa fa-spinner fa-spin"></i> Activating Setup');

                var data = {
                    _id: $(this).attr('data-setup-id'),
                    _token: "{{csrf_token()}}"
                };

                $.ajax({
                    url: '{{url('admin/setup/activate')}}',
                    data: data,
                    type: 'POST'
                })
                .done(function(response){
                    showAlert('success',response.message);
                    getList();
                })
                .fail(function(error){
                    showAlert('error',error.message);
                });
            });
        });
    </script>
@stop  
