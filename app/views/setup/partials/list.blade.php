@if( count($list) )
    @foreach($list as $record)
        <tr id="{{$record->id}}" {{($record->active == 1)? 'class="success"': ''}}>
            <td>
                <input value="{{$record->id}}" type="checkbox" class="checkbulk" />
                <!--
                <a id="updateRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Update Record"><i class="fa fa-edit"></i> </a>
                <a id="deleteRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Delete Record"><i class="fa fa-remove"></i> </a>
                -->
            </td>
            <td>{{strtoupper($record->title)}}</td>
            <td>{{date("M/d/Y",strtotime($record->start_date))}}</td>
            <td>{{($record->active == 1)? 'Setup is Activated': '<a data-setup-id="'.$record->id.'" class="btnActivateSetup btn btn-xs btn-default" href="#"><i class="fa fa-gear"></i> Activate Setup</a>'}}</td>
            <td>{{ExaminationEventSetting::countParticipant($record->id)}}</td>
        </tr>
    @endforeach
@endif