<div id="setup_examination_event" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Register new {{$recordtitle}}</h4>
            </div>
            <div class="modal-body">
                <form id="formExaminationSetupCreate" action="#" method="post" accept-charset="UTF-8" role="form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Examintion Event Title</label>
                                <input type="text" name="examination_title" id="examination_title" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Event Start Date</label>
                                <input type="date" name="start_date" id="start_date" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="activate" name="active" value="1"> Activate Setup
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnCreateSubmit" type="button" class="btn btn-success"><i class="fa fa-save"></i> Set</button>
                <button id="btnCreateReset" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#formExaminationSetupCreate").validate({
                errorClass: 'error',
                errorElement: 'span'
            });

            $("#btnCreateSubmit").on("click",function(e){
                e.preventDefault();
                e.stopPropagation();

                if( $("#formExaminationSetupCreate").valid() == true){
                    var data = {
                        title: $("#formExaminationSetupCreate").find("#examination_title").val(),
                        active: $("#formExaminationSetupCreate").find("#activate:checked").val(),
                        date: $("#formExaminationSetupCreate").find("#start_date").val(),
                        _token: "{{csrf_token()}}"
                    };

                    $.ajax({
                        url: '{{url('admin/setup')}}',
                        data: data,
                        type: 'POST'
                    })
                    .done(function(response){
                        showAlert('success',response.message);

                        $("#formExaminationSetupCreate").find("#examination_title").val("").focus();
                        $("#formExaminationSetupCreate").find("#activate").prop('checked',false);

                        $("#setup_examination_event").find(".modal-title").html("Setup New Examination");
                        $("#setup_examination_event").modal('hide');

                        getList();
                    })
                    .fail(function(error){
                        showAlert('error',error.message);
                    });
                }
            });
        });
    </script>
@stop