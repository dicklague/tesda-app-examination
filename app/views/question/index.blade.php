@section('page-view-css')
    @parent
    <style>
        .panel-list .panel-body { padding:0px; }
        .panel-list .panel-footer .pagination { margin: 0; }
        .panel-list .glyphicon,
        .panel-list .list-group-item .glyphicon { margin-right:5px; }
        .panel-list .panel-body .radio,
        .panel-list .panel-body .checkbox { display:inline-block;margin:0px; }
        .panel-list .panel-body input[type=checkbox]:checked + label { text-decoration: line-through;color: rgb(128, 144, 160); }
        .panel-list .list-group-item:hover,
        .panel-list .list-group-item:focus {text-decoration: none;background-color: rgb(245, 245, 245);}
        .panel-list .list-group { margin-bottom:0px; }
        .panel-list .list-group { border-radius: 0px; -moz-border-radius: 0px; -o-border-radius: 0px; -webkit-border-radius: 0px; }
        .panel-list .list-group .list-group-item { border-left: none medium transparent; border-right: none medium transparent; }
        .panel-list .list-group .list-group-item:last-child,
        .panel-list .list-group .list-group-item:first-child{ border-radius: 0px; -moz-border-radius: 0px; -o-border-radius: 0px; -webkit-border-radius: 0px; }
        .panel-list .list-group .list-group-item:first-child{ border-top: none medium transparent;}
        .panel-list .list-group .list-group-item:last-child{ border-bottom: none medium transparent;}
    </style>
@stop

<div class="row">
    <div class="col-md-4">
        @include('question.partials.specialization')
    </div>
    <div class="col-md-8">
        @include($page_content_view)
    </div>
</div>
@section('page-view-js')
    @parent
    <script type="text/javascript" src="{{asset('assets/plugins/mathjaxlibrary/MathJax.js?config=TeX-AMS-MML_HTMLorMML')}}"></script>
@stop