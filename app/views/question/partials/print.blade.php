<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            @media all{
                * {
                    padding: 0px;
                    margin: 0px;
                    background: #FFFFFF;
                    color: #000000;
                    font-family: "verdana", "sans-serif";
                }

                .row-print{
                    margin: 10px auto;
                }

                .numbering, .question{
                    float: left;
                }

                .numbering{
                    width: 5%;
                    font-size: 12pt;
                }

                .question{
                    width: 94%;
                    font-size: 12pt;
                }

                .clear{
                    clear: both;
                }

                .center{
                    text-align: center;
                }

                .option{
                    padding-left: 25px;
                }
            }
            @media print {
                .row-print{
                    page-break-inside: avoid;
                }
            }
        </style>
    </head>
    <body>
        <h3 class="center">{{$specialization->getSpecialization()}}</h3>
        <hr />
        <div class="question-body">
            @if(count($qid) > 0)
                <?php $count = 1; ?>
                @foreach($qid as $question)
                    <div class="row-print">
                        <div class="numbering">{{$count++}}.</div>
                        <div class="question">
                            <div class="text">{{$question->question}}</div>
                            <div class="option">
                                <ol type="a">
                                    @foreach($question->choices as $choices)
                                        <li @if($choices->answer == 1) style="color: #0000FF;" @endif >{{$choices->option}}</li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                @endforeach
            @endif
        </div>
    </body>
</html>