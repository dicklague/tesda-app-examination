@if(isset($list) && count($list) > 0)
    @foreach($list as $record)
        <tr id="{{$record->id}}">
            <td>
                <input value="{{$record->id}}" type="checkbox" class="checkbulk" />
                <a id="updateRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Update Record"><i class="fa fa-edit"></i> </a>
                <a id="deleteRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Delete Record"><i class="fa fa-remove"></i> </a>
            </td>
            <td>{{Specialization::getName($record->specializations_id)}}</td>
            <td>{{$record->question}}</td>
            <td>{{$record->answer()}}</td>
        </tr>
    @endforeach
@endif
