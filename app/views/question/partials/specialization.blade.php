<div id="fieldSpecialization" class="panel panel-list panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Field of Specialization</h3>
    </div>
    <div class="panel-body">
        <div class="list-group">

            @foreach($fields as $specialization)
                <a href="{{url('admin/questions/'.$page.'/'.$specialization->id)}}" class="sp-btn-link list-group-item @if($specialization->id == $active) active @endif">
                    <span class="badge">{{$specialization->question->count()}}</span>
                    <div class="checkbox">
                        <input class="field-specialization" data-link="{{url('admin/questions/'.$page.'/'.$specialization->id)}}" type="radio" id="field_{{$specialization->id}}" name="field" value="{{$specialization->id}}" @if($specialization->id == $active) checked="checked" @endif />
                        <label for="field_{{$specialization->id}}">
                            {{FieldCategory::fieldname($specialization->field_category_id)}} - {{$specialization->title}}
                        </label>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</div>

@section('page-view-js')
	@parent
	<script>
		$(function(e){
			$("input[type='radio'].field-specialization").on("change",function(e){
                e.preventDefault();
                e.stopPropagation();

                var link = $(this).closest('a').attr('href');

                $(".sp-btn-link").each(function(){
                    $(this).removeClass('active');
                });
                $("input[type='radio'].field-specialization").each(function(){
                    $(this).prop('checked',false);
                });
                $(this).prop('checked',true);

                $(this).closest('a').addClass('active');

                window.location.href = link;
			});
            $(".sp-btn-link").on("click",function(e){
                e.preventDefault();
                e.stopPropagation();

                var link = $(this).attr('href');
                $(".sp-btn-link").each(function(){
                    $(this).removeClass('active');
                });
                $(this).find("input[type='radio'].field-specialization").prop("checked",true);
                $(this).addClass('active');

                window.location.href = link;
            });
		});
	</script>
@stop
