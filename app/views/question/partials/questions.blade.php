<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{$recordtitle}}</h3>
        <a href="#" id="printTestQuestion" data-placement="top" data-toggle="tooltip" title="Print Selected Test Question" class="pull-right clickable"><i class="glyphicon glyphicon-print"></i></a>
        <a href="{{url('admin/questions/create/'.$active)}}" data-placement="top" data-toggle="tooltip" title="Create New Question" class="pull-right clickable"><i class="glyphicon glyphicon-plus"></i></a>
        <a class="hide" gref="#" data-placement="top" data-toggle="tooltip" title="Delete selected question" class="pull-right clickable"><i data-toggle="modal" data-target="#deleteRecord"class="glyphicon glyphicon-trash"></i></a>
    </div>
    <div class="panel-body table-container">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <th>
                        <input type="checkbox" id="checkall" />
                    </th>
                    <th>Specialization</th>
                    <th>Question</th>
                    <th>Answer</th>
                </thead>
                <tbody id="question_list_all-result"></tbody>
            </table>
        </div>
    </div>
</div>


@include('modal.confirmdelete')

@section('page-view-js')
    @parent
    <script type="text/javascript" src="{{asset('assets/plugins/mathjaxlibrary/MathJax.js?config=TeX-AMS-MML_HTMLorMML')}}"></script>
    <script>
        $(function(){
            getList();
        });

        function getList(){
            $("#question_list_all-result").load("{{url('admin/questions/list/'.$active)}}");
            return false;
        }

        $(function(){
            $(document).on("click","#deleteRecord",function(e){
                e.preventDefault();
                var record_id = $(this).attr('data-record-id');
                var con = confirm("Are You sure, you want to delete this record?");
                if(con){
                    $.post("{{url('admin/questions/delete')}}/"+record_id,{ _token: "{{csrf_token()}}"},function(response){
                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                        } else {
                            showAlert('error',response.message);
                            $("tr#"+response.data).find("td").css('background',"#cbcbcb")
                        }
                    });
                }
            });

            $(document).on("click","#updateRecord",function(e){
                e.preventDefault();
                var record_id = $(this).attr('data-record-id');
                window.location.href = "{{url('admin/questions/update/'.$active)}}/"+record_id;
            });

            $(document).on("click","#printTestQuestion",function(e){
                e.preventDefault();
                var selectedItem = new Array();

                $('#question_list_all-result input.checkbulk:checked').each(function(){
                    selectedItem.push( $(this).val() );
                });

                if(selectedItem.length === 0){
                    showAlert("error","Please Select Questions to be printed out.");
                } else {
                    var id = selectedItem.toString();
                    var url = "{{url('admin/questions/print')}}?fld={{$active}}&qid=" + id +"&_token={{csrf_token()}}";

                    $("iframe#printPage").remove();
                    $('<iframe id="iframeprint" name="printPage" src=' + url + ' style="display: none; @media print { display: block; }"></iframe>').insertAfter('body');
                    callPrint('iframeprint');
                }
            });

            //initiates print once content has been loaded into iframe
            function callPrint(iframeId) {
                var PDF = document.getElementById(iframeId);
                PDF.focus();
                PDF.contentWindow.print();
            }
        });
    </script>
@stop