    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create {{$recordtitle}} for <b><u>{{Specialization::getName($active)}}</u></b></h3>
            <a id="saveQuestion" href="#" data-placement="top" data-toggle="tooltip" title="Save Created Question" class="pull-right clickable"><i class="glyphicon glyphicon-save"></i> Save Question </a>
            <a id="resetQuestion" href="#" data-placement="top" data-toggle="tooltip" title="Reset Question Entry" class="pull-right clickable"><i class="glyphicon glyphicon-refresh"></i> Reset Entries</a>
        </div>
        <div class="panel-body">
            <form id="formQuestionCreate" action="{{isset($data) ? url('admin/questions/update/'.$active.'/'.$data->id) : url('admin/questions/create/'.$active)}}" method="post" accept-charset="UTF-8" role="form">
                {{Form::token()}}
                <input type="hidden" name="field_of_specialization" id="field_of_specialization" value="{{$active}}">
                @if(isset($data))
                    <input type="hidden" name="question_id" id="question_id" value="{{$data->id}}"/>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea required name="question" id="question">{{isset($data)? $data->question:''}}</textarea>
                        </div>
                    </div>
                </div>

                @if(isset($data))
                    @foreach($data->choices as $option)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" name="answer" id="answer_{{$option->id}}" class="answer" value="{{$option->id}}" @if($option->answer == 1) checked="checked" @endif />
                                    </span>
                                    <input type="text" class="form-control option" name="option[{{$option->id}}]" id="option_{{$option->id}}" value="{{$option->option}}">
                                </div>
                            </div>
                        </div>
                        <br />
                    @endforeach
                    @for($q=0;$q<(4 - count($data->choices));$q++)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" name="answer" id="answer_{{$q}}" class="answer" value="{{$q}}">
                                    </span>
                                        <input type="text" class="form-control option" name="option[new_{{$q}}]" id="option_{{$q}}" value="">
                                    </div>
                                </div>
                            </div>
                            <br />
                    @endfor
                @else
                    @for($q=0;$q<4;$q++)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" name="answer" id="answer_{{$q}}" class="answer" value="{{$q}}">
                                    </span>
                                    <input type="text" class="form-control option" name="option[{{$q}}]" id="option_{{$q}}" value="">
                                </div>
                            </div>
                        </div>
                        <br />
                    @endfor
                @endif
            </form>
        </div>
    </div>

@section('page-view-js')
    @parent
    <script type="text/javascript" src="{{asset('assets/plugins/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript"><!--
        CKEDITOR.replace('question', {
            extraPlugins: 'mathjax,font'
        });
        //--></script>
    <script>
        $(function(){
            $(document).on("click","#resetQuestion",function(e){
                $("#formQuestionCreate").trigger('reset');
            });

            $(document).on("click",".checkbox",function(e){
                $("a.list-group-item").removeClass('active');
                $(this).find("input[type='checkbox']").prop('checked',true);
                $(this).closest('a').addClass('active');
            });

            $(document).on("click","#saveQuestion",function(e){
                e.preventDefault();
                //var question = $("#question").val().trim();
                var question = CKEDITOR.instances['question'].getData();

                var option = $("input.option");
                var option_count = 0;
                $.each(option,function(){
                    if( $(this).val().trim() != "" ) { option_count++; }
                });
                var answer = $("input.answer:checked");

                if( question == null || question == ""){
                    showAlert("error","Question is empty");
                } else {
                    question = question.replace('<p>', " ");
                    question = question.replace('</p>', " ");
                    question=question.replace(/&nbsp;/gi," ");
                    question = question.trim();
                    if( question == null || question == ""){
                        showAlert("error","Question is empty");
                    } else {
                        if( option_count < 2){
                            showAlert("error","Question must have at least two option");
                        } else {
                            if( answer.length < 1){
                                showAlert("error","Answer to question must be required");
                            } else {
                                $("#formQuestionCreate").submit();
                            }
                        }
                    }
                }

            });
        });
    </script>
@stop