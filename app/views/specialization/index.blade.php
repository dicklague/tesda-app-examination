<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Field Of Specialization</h3>
                <a id="createNeSpecialization" href="#" data-placement="top" data-toggle="tooltip" title="Create new Field of Specialization record" class="pull-right clickable"><i class="glyphicon glyphicon-plus"></i></a>
                <a href="#" data-placement="top" data-toggle="tooltip" title="Delete selected Field of Specialization " class="pull-right clickable hide"><i data-toggle="modal" data-target="#deleteRecord"class="glyphicon glyphicon-trash"></i></a>
            </div>
            <div class="panel-body table-container">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <th>
                            <input type="checkbox" id="checkall" />
                        </th>
                        <th>NC Category</th>
                        <th>Name</th>
                        <th>Description</th>
                        </thead>
                        <tbody id="field-list-result"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('specialization.partials.create')
@include('modal.confirmdelete')

@section('page-view-js')
    @parent
    <script>
        $(function(){
            getList();
        });

        function getList(){
            $("#field-list-result").load("{{url('admin/fields/list')}}");
            return false;
        }

        $(function(){
            $(document).on("click","#deleteRecord",function(e){
                e.preventDefault();
                var record_id = $(this).attr('data-record-id');
                var con = confirm("Are You sure, you want to delete this record?");
                if(con){
                    $.post("{{url('admin/fields/delete')}}/"+record_id,{ _token: "{{csrf_token()}}"},function(response){
                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                        } else {
                            showAlert('error',response.message);
                        }
                    });
                }
            });

            $(document).on("click","#updateRecord",function(e){
                e.preventDefault();
                var id = $(this).attr('data-record-id');
                $.get("{{url('admin/fields/record')}}",{ id: id, _token: '{{csrf_token()}}'},function(response){
                    if(response.status == true) {
                        $("#specializationId").val(response.data.id);
                        $("#category").val(response.data.field_category_id);
                        $("#fieldname").val(response.data.title);
                        $("#description").val(response.data.description);

                        $("#create_specialization").find(".modal-title").html("Update Specialization Record");
                        $("#create_specialization").find("#btnCreateSubmit").html('<i class="fa fa-save"></i> Save Update');
                        $("#create_specialization").modal('show');
                    } else {
                        showAlert('error',response.message);
                    }
                });
            });

            $(document).on("click","#createNeSpecialization",function(e){
                e.preventDefault();
                $("#specializationId").val('');
                $("#category option:first-child").attr('selected');
                $("#fieldname").val('');
                $("#description").val('');

                $("#create_specialization").find(".modal-title").html("Add New Specialization");
                $("#create_specialization").find("#btnCreateSubmit").html('<i class="fa fa-save"></i> Save Specialization');
                $("#create_specialization").modal('show');
            });
        });
    </script>
@stop  