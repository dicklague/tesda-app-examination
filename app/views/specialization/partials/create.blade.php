<div id="create_specialization" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create new {{$recordtitle}}</h4>
            </div>
            <div class="modal-body">
                <form id="formSpecializationCreate" action="#" method="post" accept-charset="UTF-8" role="form">
                    <input type="hidden" name="specializationId" id="specializationId" value="" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" type="text" name="category" id="category" required>
                                    @if( count($fieldcategory) > 0)
                                        @foreach($fieldcategory as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Field of Specialization</label>
                                <input class="form-control" type="text" name="fieldname" id="fieldname" value="" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" id="description"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnCreateSubmit" type="button" class="btn btn-success"><i class="fa fa-save"></i> Create</button>
                <button id="btnCreateReset" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#formSpecializationCreate").validate({
                errorClass: 'error',
                errorElement: 'span'
            });

            $(document).on("click","#btnCreateSubmit",function(e){
                if( $("#formSpecializationCreate").valid() == true){
                    var request = {
                        'id': $("#specializationId").val().trim(),
                        'category': $("#category").val().trim(),
                        'name': $("#fieldname").val().trim(),
                        'description': $("#description").val().trim(),
                        '_token': '{{csrf_token()}}'
                    };

                    $.post("{{url('admin/fields/create')}}",request,function(response){

                        if(response.status == true){
                            showAlert('success',response.message);
                            getList();
                            $.each( $("#formSpecializationCreate input"),function(){
                                $(this).val("");
                            });
                            $('#create_specialization').modal('hide');
                        } else {
                            showAlert('error',response.message);
                        }

                    });
                } else {

                }
            });
        });
    </script>
@stop