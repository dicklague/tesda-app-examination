@if( count($list) )
    @foreach($list as $record)
        <tr id="{{$record->id}}">
            <td>
                <input value="{{$record->id}}" type="checkbox" class="checkbulk" />
                <a id="updateRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Update Record"><i class="fa fa-edit"></i> </a>
                <a id="deleteRecord" data-record-id="{{$record->id}}" href="#" data-placement="top" data-toggle="tooltip" title="Delete Record"><i class="fa fa-remove"></i> </a>
            </td>
            <td>{{FieldCategory::fieldname($record->field_category_id)}}</td>
            <td>{{$record->title}}</td>
            <td>{{$record->description}}</td>
        </tr>
    @endforeach
@endif  
