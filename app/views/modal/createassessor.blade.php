<div id="create_asessor" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create new {{$recordtitle}}</h4>
            </div>
            <div class="modal-body">
                <form id="formAssessorCreate" action="#" method="post" accept-charset="UTF-8" role="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Firstname</label>
                                <input class="form-control" type="text" name="firstname" id="firstname" value="" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lastname</label>
                                <input class="form-control" type="text" name="firstname" id="firstname" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Field of Specialization</label>
                                <input class="form-control" type="text" name="firstname" id="firstname" value="" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="form-control" type="text" name="firstname" id="firstname" value="" />
                            </div>
                        </div>
                    </div><div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact(mobile/phone)</label>
                                <input class="form-control" type="text" name="firstname" id="firstname" value="" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnCreateSubmit" type="button" class="btn btn-success"><i class="fa fa-save"></i> Create</button>
                <button id="btnCreateReset" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('page-view-js')
    @parent
    <script>
        $(function(){
            $("#formAssessorCreate").validate({
                errorClass: 'error',
                errorElement: 'span'
            });


        });
    </script>
@stop